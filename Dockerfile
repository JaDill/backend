FROM registry.krsq.me:5000/wingle/php-fpm

COPY composer.* ./

RUN composer install --no-autoloader --no-dev

COPY . ./

ARG DB_PASSWORD
ARG MAIL_PASSWORD
ARG PUSHER_APP_KEY
ARG PUSHER_APP_SECRET
ARG APP_KEY
ARG PASETO_AUTH_KEY

RUN composer dump-autoload -o \
    && sed \
      "s~DB_PASSWORD=.*~DB_PASSWORD=${DB_PASSWORD}~; \
       s~MAIL_PASSWORD=.*~MAIL_PASSWORD=${MAIL_PASSWORD}~; \
       s~PUSHER_APP_KEY=.*~PUSHER_APP_KEY=${PUSHER_APP_KEY}~; \
       s~PUSHER_APP_SECRET=.*~PUSHER_APP_SECRET=${PUSHER_APP_SECRET}~; \
       s~PASETO_AUTH_KEY=.*~PASETO_AUTH_KEY=${PASETO_AUTH_KEY}~; \
       s~APP_KEY=.*~APP_KEY=${APP_KEY}~;" .env.production > .env \
    && rm .env.production \
    && php artisan auth:key \
    && chown -R 82 bootstrap/cache/ \
    && chown -R 82 storage/