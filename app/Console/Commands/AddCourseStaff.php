<?php

namespace App\Console\Commands;

use App\Models\Course;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AddCourseStaff extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'course:add-staff {id} {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add staff by email to given course';

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $course = Course::find($this->argument('id'));

        if($course === null) {
            $this->error('Cannot find course with given id');

            return;
        }

        $exists = DB::table('course_staff')
            ->where('course_id', $course->id)
            ->where('user_id', $this->argument('email'))
            ->exists();

        if($exists) {
            $this->error('Staff with given email already has access to this course');

            return;
        }

        DB::table('course_staff')->insert(['course_id' => $course->id, 'user_id' => $this->argument('email')]);

        $this->info("Staff with given email has been granted access to course wih id {$course->id}");
    }
}
