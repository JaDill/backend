<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use ParagonIE\Paseto\Keys\SymmetricKey;

class CreateAuthKey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth:key {--show : Display the key instead of modifying files}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates new authentication key for PASETO';

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $key = (new SymmetricKey(random_bytes(32)))->encode();

        if($this->option('show')) {
            $this->line('<comment>'.$key.'</comment>');

            return;
        }

        if (!file_exists($env_path = base_path('.env'))) {
            $this->error("Could not find env file!");

            return;
        }

        $this->updateEnvFile($env_path, $key);
    }

    protected function updateEnvFile($path, $key)
    {
        $oldContent = file_get_contents($path);

        $search = $this->getEnvKey() . '=' . env($this->getEnvKey(), '');

        $newValue = $this->getEnvKey() . '=' . $key;

        if(str_contains($oldContent, $search)) {
            if(strlen(env($this->getEnvKey(), '')) === 0 || $this->confirm($this->keyExistsQuestion())) {
                $newContent = str_replace($search, $newValue, $oldContent);
            } else {
                return;
            }
        } else {
            $eol = substr($oldContent, -1) === PHP_EOL ?: PHP_EOL . PHP_EOL;
            $newContent = $oldContent . $eol . $newValue;
        }

        file_put_contents($path, $newContent);

        $this->info(".env file has been updated with new auth key!");
    }

    protected function getEnvKey() {
        return 'PASETO_AUTH_KEY';
    }

    protected function keyExistsQuestion() {
        return 'Looks like you have already set the auth key.' . PHP_EOL .
            ' Are you sure you want to replace it with the new one?' . PHP_EOL .
            ' It will lead to invalidation of all existing tokens.';
    }
}
