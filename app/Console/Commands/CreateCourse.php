<?php

namespace App\Console\Commands;

use App\Services\CourseService;
use Illuminate\Console\Command;

class CreateCourse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'course:create {title} {regex}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new course';

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $courseService = new CourseService();

        $course = $courseService->create([
            'title' => $this->argument('title'),
            'group_regex' => $this->argument('regex')
        ]);

        $this->info("New course has been successfully created with id = {$course->id}");
    }
}
