<?php

namespace App\Console\Commands;

use App\Models\Course;
use Illuminate\Console\Command;

class DeleteCourse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'course:delete {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete existing course';

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        /** @var Course $course */
        $course = Course::find($this->argument('id'));

        if($course === null) {
            $this->error('Cannot find course with given id');

            return;
        }

        $course->delete();

        $this->info("Course has been successfully deleted!");
    }
}
