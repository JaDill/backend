<?php

namespace App\Console\Commands;

use App\Models\Course;
use Illuminate\Console\Command;

class UpdateCourse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'course:update {id} {title?} {regex?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update existing course';

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        if(!$this->hasArgument('title') && !$this->hasArgument('regex')) {
            $this->error('You must provide at least one property value of course object');
        }

        $course = Course::find($this->argument('id'));

        if($course === null) {
            $this->error('Cannot find course with given id');

            return;
        }

        if($this->hasArgument('title')) {
            $course->title = $this->argument('title');
        }

        if($this->hasArgument('regex')) {
            $course->title = $this->argument('regex');
        }

        $this->info("Course with id = {$course->id} has been successfully updated!");
    }
}
