<?php

namespace App\Events;

use App\Models\Category;
use Illuminate\Queue\SerializesModels;

class CategoryCreating
{
    use SerializesModels;

    public $category;

    /**
     * Create a new event instance.
     *
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }
}
