<?php

namespace App\Events;

use App\Models\Submission;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SubmissionStatusUpdated implements ShouldBroadcast
{
    use Dispatchable, SerializesModels;

    private $submission;

    private $oldStatus;

    public function __construct(Submission $submission, string $oldStatus)
    {
        $this->submission = $submission;

        $this->oldStatus = $oldStatus;
    }

    public function broadcastWith()
    {
        return [
            'submission' => $this->submission->makeHidden(['form', 'answers', 'replies']),
            'previous_status' => $this->oldStatus
        ];
    }

    public function broadcastOn()
    {
        $userId = $this->submission->user_id;

        return new PrivateChannel("user.{$userId}");
    }
}
