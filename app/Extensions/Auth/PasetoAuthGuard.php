<?php

namespace App\Extensions\Auth;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;
use InvalidArgumentException;
use LogicException;
use ParagonIE\Paseto\Builder;
use ParagonIE\Paseto\Exception\InvalidKeyException;
use ParagonIE\Paseto\Exception\InvalidPurposeException;
use ParagonIE\Paseto\Exception\PasetoException;
use ParagonIE\Paseto\Keys\SymmetricKey;
use ParagonIE\Paseto\Parser;
use ParagonIE\Paseto\Protocol\Version2;
use ParagonIE\Paseto\ProtocolCollection;
use ParagonIE\Paseto\Purpose;
use ParagonIE\Paseto\Rules\IssuedBy;
use ParagonIE\Paseto\Rules\NotExpired;
use TypeError;

class PasetoAuthGuard implements Guard
{
    /**
     * @var UserProvider $provider
     */
    private $provider;

    /**
     * @var Request $request
     */
    private $request;

    /**
     * @var User|null user
     */
    private $user = null;

    /**
     * PasetoAuthGuard constructor.
     * @param UserProvider $provider
     * @param Request $request
     * @throws PasetoException
     */
    public function __construct(UserProvider $provider, Request $request)
    {
        $this->provider = $provider;

        $this->request = $request;

        $this->handleRequest();
    }

    /**
     * Handle incoming request, check if it contains token and
     * retrieve user from database if needed.
     *
     * @return void
     * @throws PasetoException
     */
    private function handleRequest() : void
    {
        if($this->getTokenFromRequest() === null) {
            return;
        }

        $parser = Parser::getLocal($this->getSharedKey(), ProtocolCollection::v2());

        // TODO Invalidate tokens by IssuedAt < *timestamp from db*

        $parser->addRule(new NotExpired);
        $parser->addRule(new IssuedBy($this->getIssuer()));

        try {
            $token = $parser->parse($this->getTokenFromRequest());
        } catch (PasetoException $e) {
            return;
        }

        // $this->setUser($this->provider->retrieveById($token->getSubject()));

        $this->setUser(new User($token->getClaims()));
    }

    /**
     * Login as given user instance.
     * @param string $user
     * @return string
     * @throws PasetoException
     * @throws InvalidKeyException
     * @throws InvalidPurposeException
     */
    public function login(string $user) : string
    {
        if(($user = $this->provider->retrieveById($user)) === null) {
            throw new InvalidArgumentException("Invalid user credentials provided");
        }

        $this->setUser($user);

        return $this->generateTokenForUser();
    }

    /**
     * Generate token for user stored in $this->user.
     * @return string
     * @throws PasetoException
     * @throws InvalidKeyException
     * @throws InvalidPurposeException
     */
    private function generateTokenForUser() : string
    {
        if(!($this->user instanceof User)) {
            throw new LogicException("Cannot generate token for given user.");
        }

        $token = (new Builder)
            ->setKey($this->getSharedKey())
            ->setVersion(new Version2)
            ->setPurpose(Purpose::local())
            ->setExpiration(Carbon::now()->addHours($this->getExpireTime()))
            ->setIssuer($this->getIssuer())
            ->setSubject($this->id())
            ->setClaims($this->user->toArray());

        return (string) $token;
    }

    private function getExpireTime() : string
    {
        return env('PASETO_AUTH_EXPIRE_AFTER_HOURS', '168');
    }

    /**
     * @return SymmetricKey
     * @throws TypeError
     */
    private function getSharedKey() : SymmetricKey
    {
        return SymmetricKey::fromEncodedString(env('PASETO_AUTH_KEY'));
    }

    /**
     * @return mixed
     */
    private function getIssuer()
    {
        return env('PASETO_AUTH_ISSUER');
    }
    /**
     * @return string|null
     */
    private function getTokenFromRequest()
    {
        if ($token = $this->request->headers->get('Authorization')) {
            return str_after($token, 'Bearer ');
        }

        return null;
    }

    /**
     * Determine if the current user is authenticated.
     *
     * @return bool
     */
    public function check()
    {
        return $this->user !== null;
    }

    /**
     * Determine if the current user is a guest.
     *
     * @return bool
     */
    public function guest()
    {
        return !$this->check();
    }

    /**
     * Get the currently authenticated user.
     *
     * @return User|null
     */
    public function user() : ?User
    {
        return $this->user;
    }

    /**
     * Get the ID for the currently authenticated user.
     *
     * @return int|null
     */
    public function id()
    {
        if($this->user === null) {
            return null;
        }

        return $this->user->getAuthIdentifier();
    }

    /**
     * Validate a user's credentials.
     *
     * @param  array $credentials
     * @return bool
     */
    public function validate(array $credentials = [])
    {
        return $this->provider->validateCredentials($this->user, $credentials);
    }

    /**
     * Set the current user.
     *
     * @param Authenticatable $user
     * @return void
     */
    public function setUser(Authenticatable $user)
    {
        $this->user = $user;
    }

    /**
     * Set the request instance.
     *
     * @param Request $request
     * @throws PasetoException
     */
    public function setRequest(Request $request) {
        $this->request = $request;

        // Request instance has been changed, so we should reset user.
        $this->user = null;

        $this->handleRequest();
    }
}