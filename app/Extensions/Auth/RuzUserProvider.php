<?php
namespace App\Extensions\Auth;

use App\Models\Course;
use App\Models\User;
use App\Models\UserSettings;
use App\Services\RuzApiService;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use InvalidArgumentException;

class RuzUserProvider implements UserProvider
{
    private $api;

    public function __construct(RuzApiService $apiService)
    {
        $this->api = $apiService;
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param mixed $identifier
     * @return User|null
     */
    public function retrieveById($identifier) : ?User
    {
        $user = null;

        if($this->emailBelongsToStudent($identifier)) {
            $user = User::createStudent($this->api->getStudentInfo($identifier));
        }

        if($this->emailBelongsToStaff($identifier)) {
            $user = User::createStaff($identifier);
        }

        if($user === null) {
            return null;
        }

        if(UserSettings::query()->where('user_id', $user->id)->doesntExist()) {
            UserSettings::create([
                'user_id' => $user->id,
                'default_course' => $user->detectDefaultCourse()->id ?? Course::first()
            ]);
        }

        return $user;
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     * @internal
     * @param mixed $identifier
     * @param string $token
     * @return null
     */
    public function retrieveByToken($identifier, $token)
    {
        return null;
    }

    /**
     * Update the "remember me" token for the given user in storage.
     * @internal
     * @param Authenticatable $user
     * @param string $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        return;
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param array $credentials
     * @return User|null
     */
    public function retrieveByCredentials(array $credentials) : ?User
    {
        if(sizeof($credentials) != 1 || !isset($credentials['id'])) {
            throw new InvalidArgumentException("Argument must contain only 'email' key");
        }

        return $this->retrieveById($credentials['id']);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param Authenticatable $user
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        if(!($user instanceof User)) {
            throw new InvalidArgumentException("1nd argument must be of type" . User::class);
        }

        if(sizeof($credentials) != 1 || !isset($credentials['id'])) {
            throw new InvalidArgumentException("2nd argument must contain only 'email' key");
        }

        return $user->id === $credentials['id'];
    }

    private function emailBelongsToStudent(string $email) : bool
    {
        if(preg_match('/^.+@edu\.hse\.ru$/', $email)) {
            return true;
        }

        return false;
    }

    private function emailBelongsToStaff(string $email) : bool
    {
        if(preg_match('/^.+@hse\.ru$/', $email)) {
            return true;
        }

        return false;
    }
}