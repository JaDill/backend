<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Attachment;
use App\Models\Submission;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class AttachmentController extends Controller
{
    public function update(Request $request, Attachment $attachment)
    {
        $validated = $this->validate($request, [
            'name' => [
                'required_without:file',
                'string',
                'min:2',
                'max:100',
                'regex:/^[^%\?\/!\\#\$\^]+\.(jpg|png|jpeg|doc|xls|xlsx|docx|pdf)$/'
            ],
            'file' => 'required_without:name|file|mimes:jpg,jpeg,png,pdf,docx,doc,xls,xlsx|max:' . 5 * 1024
        ]);

        if($request->user()->staff() && !$request->user()->staffHasAccessToAttachment($attachment)) {
            abort(403);
        }

        if($request->user()->student() && $request->user()->id !== $attachment->uploaded_by) {
            abort(403);
        }

        $attachedTo = $attachment->attachable;

        if($attachedTo instanceof Answer && $attachedTo->submission->status !== Submission::NEW) {
            throw ValidationException::withMessages([
                'attachment' => 'You cannot modify attachment that is associated with submission of type ' . $attachedTo->submission->status
            ]);
        }

        if(array_key_exists('name', $validated)) {
            $attachment->name = $validated['name'];
        }

        if(array_key_exists('file', $validated)) {
            Storage::disk()->delete($attachment->path);

            /** @var UploadedFile $file */
            $file = $validated['file'];

            $attachment->path = $file->store('attachments');
            $attachment->size = $file->getSize();
        }

        $attachment->save();

        return response()->json([
            'data' => $attachment->makeHidden('attachable')
        ]);
    }

    public function destroy(Request $request, Attachment $attachment)
    {
        if($request->user()->staff() && !$request->user()->staffHasAccessToAttachment($attachment)) {
            abort(403);
        }

        if($request->user()->student() && $request->user()->id !== $attachment->uploaded_by) {
            abort(403);
        }

        $attachedTo = $attachment->attachable;

        if($attachedTo instanceof Answer) {
            if($attachedTo->submission->status !== Submission::NEW) {
                throw ValidationException::withMessages([
                    'attachment' => 'You cannot delete attachment that is associated with submission of type ' . $attachedTo->submission->status
                ]);
            }

            if($attachedTo->field->required) {
                throw ValidationException::withMessages([
                    'attachment' => 'You cannot delete attachment that is associated with required field'
                ]);
            }

            $attachedTo->delete();
        }

        Storage::disk()->delete($attachment->path);

        $attachment->delete();

        return response()->noContent();
    }

    public function show(Request $request, Attachment $attachment)
    {
        if($request->user()->staff() && !$request->user()->staffHasAccessToAttachment($attachment)) {
            abort(403);
        }

        if($request->user()->student() && $request->user()->id !== $attachment->uploaded_by) {
            abort(403);
        }

        return response()->json([
            'data' => $attachment
        ]);
    }

    public function download(Request $request, Attachment $attachment)
    {
        if($request->user()->staff() && !$request->user()->staffHasAccessToAttachment($attachment)) {
            abort(403);
        }

        if($request->user()->student() && $request->user()->id !== $attachment->uploaded_by) {
            abort(403);
        }

        return Storage::disk()->download($attachment->path, $attachment->name);
    }

    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            'file' => 'required|file|mimes:jpg,jpeg,png,pdf,docx,doc,xls,xlsx|max:' . 5 * 1024,
            'name' => [
                'required',
                'string',
                'min:2',
                'max:100',
                'regex:/^[^%\?\/!\\#\$\^]+\.(jpg|png|jpeg|doc|xls|xlsx|docx|pdf)$/'
            ]
        ]);

        /** @var UploadedFile $file */
        $file = $validated['file'];

        $data = [
            'name' => $validated['name'],
            'path' => $file->store('attachments'),
            'size' => $file->getSize(),
            'uploaded_by' => $request->user()->id
        ];

        return response()->json([
            'data' => Attachment::create($data)
        ]);
    }
}
