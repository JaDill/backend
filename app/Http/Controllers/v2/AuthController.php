<?php

namespace App\Http\Controllers\v2;

use App\Extensions\Auth\RuzUserProvider;
use App\Models\Course;
use App\Models\UserSettings;
use App\Services\EmailConfirmation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function email(Request $request, EmailConfirmation $emailConfirmation, RuzUserProvider $userProvider) {
        $validated = $this->validate($request, [
            'email' => [
                'required',
                'email',
                'regex:/^.+@(edu\.)?hse\.ru$/'
            ]
        ]);

        // TODO Validate staff email using course_staff and reject request with unknown email address

        if($userProvider->retrieveById($validated['email']) === null) {
            throw ValidationException::withMessages([
                'email' => 'This email does not belong to any student'
            ]);
        }

        $emailConfirmation->send($validated['email']);

        return response()->noContent();
    }

    public function code(Request $request, EmailConfirmation $emailConfirmation) {
        $validated = $this->validate($request, [
            'email' => [
                'required',
                'email',
                'regex:/^.+@(edu\.)?hse\.ru$/'
            ],
            'code' => 'required|digits:4'
        ]);

        if(!$emailConfirmation->pending($validated['email'])) {
            throw ValidationException::withMessages([
                'email' => 'You must send code to this email first'
            ]);
        }

        if(!$emailConfirmation->check($validated['email'], $validated['code'])) {
            throw ValidationException::withMessages([
                'code' => 'Wrong code'
            ]);
        }

        $token = auth()->login($validated['email']);

        $settings = UserSettings::find($request->user()->id);

        return response([
            'data' => [
                'user' => $request->user()->toArray(),
                'settings' => $settings,
                'token' => $token
            ]
        ]);
    }

    public function resend(Request $request, EmailConfirmation $emailConfirmation) {
        $validated = $this->validate($request, [
            'email' => [
                'required',
                'email',
                'regex:/^.+@(edu\.)?hse\.ru$/'
            ]
        ]);

        if(!$emailConfirmation->pending($validated['email'])) {
            throw ValidationException::withMessages([
                'email' => 'You must send code to this email first'
            ]);
        }

        $emailConfirmation->send($validated['email']);

        return response()->noContent();
    }
}
