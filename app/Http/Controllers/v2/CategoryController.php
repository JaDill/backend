<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Category;
use App\Models\Form;
use App\Models\Reply;
use App\Models\Submission;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class CategoryController extends Controller
{
    public function index(Request $request) {
        $validated = $this->validate($request, [
            'course_id' => 'required|integer|min:1|exists:courses,id',
            'page' => 'integer|min:1',
            'page_size' => 'integer|min:1|max:100'
        ]);

        if($request->user()->staff() && !$request->user()->staffHasAccessToCourse($validated['course_id'])) {
            abort(403);
        }

        $query = Category::query()->where('course_id', $validated['course_id'])->orderBy('order');

        if($request->user()->student()) {
            $query->where(function(Builder $query) use ($request) {
                $query->where('restricted', '=', false);

                if(($course = $request->user()->studentCourse()) !== null) {
                    $query->orWhere('course_id', '=', $course->id);
                }
            });
        }

        $data = $query->paginate($request->input('page_size', 10));

        return response()->json([
            'page' => $data->currentPage(),
            'last_page' => $data->lastPage(),
            'page_size' => $data->perPage(),
            'total_records' => $data->total(),
            'data' => $data->items()
        ]);
    }

    public function show(Request $request, Category $category)
    {
        if($request->user()->staff() && !$request->user()->staffHasAccessToCategory($category)) {
            abort(403);
        }

        if($request->user()->student() && !$request->user()->studentHasAccessToCategory($category)) {
            abort(403);
        }

        return response()->json([
            'data' => $category
        ]);
    }

    public function store(Request $request) {
        if($request->user()->student()) {
            abort(403);
        }

        $validated = $this->validate($request, [
            'title' => 'required|string|min:5|max:250',
            'description' => 'string|min:5|max:250',
            'restricted' => 'required|boolean',
            'course_id' => 'required|integer|min:1|exists:courses,id'
        ]);

        if($request->user()->staff() && !$request->user()->staffHasAccessToCourse($validated['course_id'])) {
            abort(403);
        }

        return response()->json([
            'data' => Category::create($validated)
        ]);
    }

    public function update(Category $category, Request $request) {
        if($request->user()->student()) {
            abort(403);
        }

        if($request->user()->staff() && !$request->user()->staffHasAccessToCategory($category)) {
            abort(403);
        }

        $validated = $this->validate($request, [
            'title' => 'string|min:5|max:250',
            'description' => 'string|min:5|max:250',
            'restricted' => 'boolean',
            'order' => 'numeric|unique:categories,order'
        ]);

        if(sizeof($validated) === 0) {
            throw ValidationException::withMessages([
                'fields' => 'You must provide at least one parameter to update the form'
            ]);
        }

        $category->update($validated);

        return response()->json([
            'data' => $category
        ]);
    }

    public function destroy(Request $request, Category $category) {
        if($request->user()->student()) {
            abort(403);
        }

        if($request->user()->staff() && !$request->user()->staffHasAccessToCategory($category)) {
            abort(403);
        }

        if($category->trashed()) {
            throw ValidationException::withMessages([
                'category' => 'Category is already deleted'
            ]);
        }

        DB::beginTransaction();

        $category->delete();

        $category->forms->each(function(Form $form) {
            $form->delete();

            $form->fields()->delete();
        });

        DB::commit();

        return response()->noContent();
    }

    public function forceDestroy(Request $request, $category_id)
    {
        if($request->user()->student()) {
            abort(403);
        }

        if(($category = Category::withTrashed()->find($category_id)) === null) {
            abort(404);
        }

        if($request->user()->staff() && !$request->user()->staffHasAccessToCategory($category)) {
            abort(403);
        }

        if($category->trashed()) {
            throw ValidationException::withMessages([
                'category' => 'Category is already deleted'
            ]);
        }

        DB::beginTransaction();

        $category->forms->each(function(Form $form) {
            $form->submissions->each(function (Submission $submission) {
                $submission->replies->each(function(Reply $reply) {
                    $reply->attachment()->forceDelete();

                    $reply->forceDelete();
                });

                $submission->answers->each(function(Answer $answer) {
                    $answer->attachment()->forceDelete();

                    $answer->forceDelete();
                });

                $submission->forceDelete();
            });

            $form->fields()->forceDelete();

            $form->forceDelete();
        });

        $category->delete();

        DB::commit();

        return response()->noContent();
    }

    public function restore(Request $request, $trashed_category) {
        if($request->user()->student()) {
            abort(403);
        }

        $category = Category::withTrashed()->find($trashed_category);

        if($category === null) {
            abort(404);
        }

        if($request->user()->staff() && !$request->user()->staffHasAccessToCategory($category)) {
            abort(403);
        }

        if(!$category->trashed()) {
            throw ValidationException::withMessages([
                'category' => 'Category has not been deleted yet'
            ]);
        }

        $category->restore();

        return response()->noContent();
    }
}
