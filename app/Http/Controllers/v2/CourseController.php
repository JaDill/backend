<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index(Request $request) {
        $this->validate($request, [
            'query' => 'string|min:3|max:30',
            'page' => 'integer|min:1',
            'page_size' => 'integer|min:1|max:100'
        ]);

        if($request->user()->staff()) {
            $query = $request->user()->staffCoursesQuery();
        } else {
            $query = Course::query();
        }

        if($request->has('query')) {
            $query->where('courses.title', 'like', "%{$request->input('query')}%");
        }

        $data = $query->paginate($request->input('page_size', 10));

        return response()->json([
            'page' => $data->currentPage(),
            'last_page' => $data->lastPage(),
            'page_size' => $data->perPage(),
            'total_records' => $data->total(),
            'data' => $data->items()
        ]);
    }

    public function show(Request $request, Course $course)
    {
        if($request->user()->staff() && !$request->user()->staffHasAccessToCourse($course)) {
            return abort(403);
        }

        return response()->json([
            'data' => $course
        ]);
    }
}
