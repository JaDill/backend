<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Field;
use App\Models\Form;
use App\Models\Submission;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class FieldController extends Controller
{
    public function store(Request $request)
    {
        if ($request->user()->studet()) {
            abort(403);
        }

        $validated = $this->validate($request, [
            'title' => 'required|string|min:5|max:250',
            'description' => 'string|min:5|max:250',
            'form_id' => 'required|integer|min:1|exists:forms,id',
            'type' => 'required|in:' . implode(',', [Field::TYPE_SELECT, Field::TYPE_TEXT, Field::TYPE_ATTACHMENT]),
            'required' => 'required|boolean',
            'personal' => 'required|boolean',
            'meta' => 'required_if:type,' . Field::TYPE_SELECT . '|array|size:2',
            'meta.multiple' => 'required_with:meta|boolean',
            'meta.options' => 'required_with:meta|array|min:1|max:30',
            'meta.options.*' => 'required|string|distinct',
            'order' => 'required|numeric'
        ]);

        if (!$request->user()->staffHasAccessToForm($validated['form_id'])) {
            abort(403);
        }

        if (Field::query()
            ->withTrashed()
            ->where('form_id', $validated['form_id'])
            ->where('order', $validated['order'])
            ->exists()) {
            throw ValidationException::withMessages([
                'order' => 'Value must be unique for all fields with given form_id'
            ]);
        }

        if (Submission::query()
            ->where('form_id', $validated['form_id'])
            ->exists()) {
            throw ValidationException::withMessages([
                'form_id' => 'You cannot add fields to form with submissions'
            ]);
        }

        if ($validated['type'] !== Field::TYPE_TEXT && $validated['personal']) {
            throw ValidationException::withMessages([
                'personal' => 'You can only mark text fields as \'personal\''
            ]);
        }

        if ($validated['type'] !== Field::TYPE_SELECT) {
            unset($validated['meta']);
        }

        return response()->json([
            'data' => Field::create($validated)
        ]);
    }

    public function update(Field $field, Request $request)
    {
        if ($request->user()->studet()) {
            abort(403);
        }

        if (!$request->user()->staffHasAccessToField($field)) {
            abort(403);
        }

        $validated = $this->validate($request, [
            'title' => 'string|min:5|max:250',
            'description' => 'string|min:5|max:250',
            'type' => 'in:' . implode(',', [Field::TYPE_SELECT, Field::TYPE_TEXT, Field::TYPE_ATTACHMENT]),
            'required' => 'boolean',
            'personal' => 'boolean',
            'meta' => 'array|size:2',
            'meta.multiple' => 'required_with:meta|boolean',
            'meta.options' => 'required_with:meta|array|min:1|max:30',
            'meta.options.*' => 'required|string|distinct',
            'order' => 'numeric'
        ]);

        if (sizeof($validated) === 0) {
            throw ValidationException::withMessages([
                'fields' => 'You must provide at least one parameter to update the form'
            ]);
        }

        if (Field::query()
            ->withTrashed()
            ->where('form_id', $validated['form_id'])
            ->where('id', '!=', $field->id)
            ->where('order', $validated['order'])
            ->exists()) {
            throw ValidationException::withMessages([
                'order' => 'Value must be unique for all fields with given form_id'
            ]);
        }

        if (Submission::query()
            ->where('form_id', $field->form_id)
            ->exists()) {
            throw ValidationException::withMessages([
                'form_id' => 'You cannot modify field attached to form with submissions'
            ]);
        }

        if ($validated['type'] !== Field::TYPE_TEXT && $validated['personal']) {
            throw ValidationException::withMessages([
                'personal' => 'You can only mark text fields as \'personal\''
            ]);
        }

        if($field->type !== $validated['type']) {
            if($validated['type'] === Field::TYPE_SELECT && !array_key_exists('meta', $validated)) {
                throw ValidationException::withMessages([
                    'meta' => 'Meta-data is required since field type is changed to ' . Field::TYPE_SELECT
                ]);
            }
        }

        if ($validated['type'] !== Field::TYPE_SELECT) {
            unset($validated['meta']);
        }

        $field->update($validated);

        return response()->json([
            'data' => $field
        ]);
    }

    public function destroy(Request $request, Field $field)
    {
        if ($request->user()->studet()) {
            abort(403);
        }

        if (!$request->user()->staffHasAccessToField($field)) {
            abort(403);
        }

        if (Submission::query()
            ->where('form_id', $field->form_id)
            ->exists()) {
            throw ValidationException::withMessages([
                'form_id' => 'You cannot modify field attached to form with submissions'
            ]);
        }

        $field->forceDelete();

        return response()->noContent();
    }
}
