<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Category;
use App\Models\Field;
use App\Models\Form;
use App\Models\Reply;
use App\Models\Submission;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class FormController extends Controller
{
    public function index(Request $request) {
        $validated = $this->validate($request, [
            'query' => 'string|min:3|max:30',
            'category_id' => 'required_without:course_id|integer|min:1|exists:categories,id',
            'course_id' => 'required_without:category_id|integer|min:1|exists:courses,id',
            'page' => 'integer|min:1',
            'page_size' => 'integer|min:1|max:100'
        ]);

        $query = Form::query()->without(['fields']);

        if($request->has('query')) {
            $query->where('forms.title', 'like', "%{$validated['query']}%")
            ->orWhere('forms.description', 'like', "%{$validated['query']}%");
        }

        if($request->has('category_id')) {
            $category = Category::find($validated['category_id']);

            if($request->user()->staff()
                && !$request->user()->staffHasAccessToCourse($category->course_id)) {
                abort(403);
            }

            if($request->user()->student()
                && !$request->user()->studentHasAccessToCategory($category)) {
                abort(403);
            }

            $query->where('forms.category_id', $validated['category_id']);
        }

        if($request->has('course_id') || $request->user()->student()) {
            $query->join('categories', 'categories.id', '=', 'forms.category_id')
                ->select(['forms.*']);
        }

        if($request->has('course_id')) {
            if($request->user()->staff()
                && !$request->user()->staffHasAccessToCourse($validated['course_id'])) {
                abort(403);
            }

            $query->where('categories.course_id', $validated['course_id']);
        }

        if($request->user()->student()) {
            $query->where(function (Builder $query) use ($request) {
                $query->where('forms.restricted', '=', false);

                if(($course = $request->user()->studentCourse()) !== null) {
                    $query->orWhere('categories.course_id', '=', $course->id);
                }
            });
        }

        $data = $query->paginate($request->input('page_size', 10));

        return response()->json([
            'page' => $data->currentPage(),
            'last_page' => $data->lastPage(),
            'page_size' => $data->perPage(),
            'total_records' => $data->total(),
            'data' => $data->items()
        ]);
    }

    public function show(Request $request, Form $form)
    {
        if($request->user()->staff()
            && !$request->user()->staffHasAccessToForm($form)) {
            abort(403);
        }

        if($request->user()->student()
            && !$request->user()->studentHasAccessToForm($form)) {
            abort(403);
        }

        return response()->json([
            'data' => $form
        ]);
    }

    public function store(Request $request) {
        if($request->user()->student()) {
            abort(403);
        }

        $validated = $this->validate($request, [
            'title' => 'required|string|min:5|max:250',
            'description' => 'string|min:5|max:250',
            'restricted' => 'required|boolean',
            'category_id' => 'required|integer|min:1|exists:categories,id',
            'expires_after' => 'required|integer|min:1|max:90',
            'fields' => 'required|array|min:1|max:15',
            'fields.*.title' => 'required|string|min:5|max:250',
            'fields.*.description' => 'string|min:5|max:250',
            'fields.*.type' => 'required|in:' . implode(',', [Field::TYPE_SELECT, Field::TYPE_TEXT, Field::TYPE_ATTACHMENT]),
            'fields.*.required' => 'required|boolean',
            'fields.*.personal' => 'required|boolean',
            'fields.*.meta' => 'required_if:fields.*.type,' . Field::TYPE_SELECT . '|array|size:2',
            'fields.*.meta.multiple' => 'required_with:fields.*.meta|boolean',
            'fields.*.meta.options' => 'required_with:fields.*.meta|array|min:1|max:30',
            'fields.*.meta.options.*' => 'required|string|distinct',
            'fields.*.order' => 'required|numeric|distinct'
        ]);

        if($request->user()->staff()
            && !$request->user()->staffHasAccessToCategory($validated['category_id'])) {
            abort(403);
        }

        if(Category::find($validated['category_id'])->restricted && !$validated['restricted']) {
            throw ValidationException::withMessages([
                'restricted' => 'Form must be restricted because of category restriction'
            ]);
        }

        $fields = [];

        foreach ($validated['fields'] as $field) {
            if($field['type'] !== Field::TYPE_TEXT && $field['personal']) {
                throw ValidationException::withMessages([
                    'personal' => 'You can only mark text fields as \'personal\''
                ]);
            }

            if($field['type'] !== Field::TYPE_SELECT) {
                unset($field['meta']);
            }

            $fields[] = new Field($field);
        }

        /** @var Form $form */
        $form = Form::create($request->only(['title', 'description', 'restricted', 'category_id', 'expires_after']));

        $form->fields()->saveMany($fields);

        return response()->json([
            'data' => $form
        ]);
    }

    public function update(Form $form, Request $request) {
        if($request->user()->student()) {
            abort(403);
        }

        if($form->submissions()->exists()) {
            throw ValidationException::withMessages([
                'form' => 'You cannot update form which already has submissions'
            ]);
        }

        $validated = $this->validate($request, [
            'title' => 'string|min:5|max:250',
            'description' => 'string|min:5|max:250',
            'restricted' => 'boolean',
            'category_id' => 'integer|min:1|exists:categories,id',
            'expires_after' => 'integer|min:1|max:90'
        ]);

        if(sizeof($validated) === 0) {
            throw ValidationException::withMessages([
                'fields' => 'You must provide at least one parameter to update the form'
            ]);
        }

        if($request->user()->staff()) {
            if(!$request->user()->staffHasAccessToForm($form)) {
                abort(403);
            }

            if($request->has('category_id') &&
                !$request->user()->staffHasAccessToCategory($validated['category_id'])) {
                abort(403);
            }
        }

        $form->update($validated);

        return response()->json([
            'data' => $form
        ]);
    }

    public function destroy(Request $request, Form $form) {
        if($request->user()->student()) {
            abort(403);
        }

        if(!$request->user()->staffHasAccessToForm($form)) {
            abort(403);
        }

        if($form->trashed()) {
            throw ValidationException::withMessages([
                'form' => 'Form has not been deleted yet'
            ]);
        }

        $form->fields()->delete();

        $form->delete();

        return response()->noContent();
    }

    public function forceDestroy(Request $request, Form $form) {
        if($request->user()->student()) {
            abort(403);
        }

        if(!$request->user()->staffHasAccessToForm($form)) {
            abort(403);
        }

        $form->submissions->each(function (Submission $submission) {
            $submission->replies->each(function(Reply $reply) {
                $reply->attachment()->forceDelete();

                $reply->forceDelete();
            });

            $submission->answers->each(function(Answer $answer) {
                $answer->attachment()->forceDelete();

                $answer->forceDelete();
            });

            $submission->forceDelete();
        });

        $form->fields()->forceDelete();

        $form->forceDelete();

        return response()->noContent();
    }

    public function restore(Request $request, $form_id) {
        if($request->user()->student()) {
            abort(403);
        }

        if(($form = Category::withTrashed()->find($form_id)) === null) {
            abort(404);
        }

        if(!$request->user()->staffHasAccessToForm($form)) {
            abort(403);
        }

        if(!$form->trashed()) {
            throw ValidationException::withMessages([
                'form' => 'Form is already deleted'
            ]);
        }

        $form->restore();

        $form->fields()->withTrashed()->restore();

        return response()->noContent();
    }
}
