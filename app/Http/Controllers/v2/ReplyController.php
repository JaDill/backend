<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Models\Category;
use App\Models\Reply;
use App\Models\Submission;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ReplyController extends Controller
{
    public function store(Request $request) {
        if($request->user()->student()) {
            abort(403);
        }

        $validated = $this->validate($request, [
            'reply' => 'required|string|min:3|max:1000',
            'submission_id' => 'required|integer|min:1|exists:submissions,id',
            'attachment_id' => 'integer|min:1|exists:attachments,id',
        ]);

        if(!$request->user()->staffHasAccessToSubmission($validated['submission_id'])) {
            abort(403);
        }

        /** @var Reply $reply */
        $reply = Reply::create([
            'submission_id' => $validated['submission_id'],
            'reply' => $validated['reply']
        ]);

        if($request->has('attachment_id')) {
            /** @var Attachment $attachment */
            $attachment = Attachment::find($validated['attachment_id']);

            if($attachment->attachable_id !== null) {
                throw ValidationException::withMessages([
                    'attachment_id' => 'You cannot use given attachment'
                ]);
            }

            $reply->attachment()->save($attachment);
        }

        return response()->json([
            'data' => $reply
        ]);
    }
}
