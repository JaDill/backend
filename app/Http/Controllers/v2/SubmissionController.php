<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Attachment;
use App\Models\Course;
use App\Models\Field;
use App\Models\Form;
use App\Models\Submission;
use App\Services\FieldService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class SubmissionController extends Controller
{
    public function index(Request $request) {
        $validated = $this->validate($request, [
            'course_id' => 'integer|min:1|exists:courses,id',
            'category_id' => 'integer|min:1|exists:categories,id',
            'status' => 'array|min:1',
            'status.*' => 'distinct|in:' . implode(',', [Submission::NEW, Submission::IN_PROGRESS, Submission::REJECTED, Submission::COMPLETED]),
            'sort' => 'in:created_at,updated_at,delete_at',
            'order' => 'in:asc,desc',
            'page' => 'integer|min:1',
            'page_size' => 'integer|min:1|max:100'
        ]);

        $query = Submission::query()->without(['answers', 'form.fields', 'replies']);

        if($request->has('status')) {
            $query->whereIn('submissions.status', $request->input('status'));
        }

        $order = $request->input('order', 'desc');

        if($request->has('sort')) {
            $query->orderBy("submissions.{$request->input('sort')}", $order);
        } else {
            $query->orderByDesc('submissions.created_at');
        }

        if($request->user()->student()) {
            $query->where('submissions.user_id', '=', $request->user()->id);

            if($request->has('category_id') && !$request->user()->studentHasAccessToCategory($validated['category_id'])) {
                abort(403);
            }
        }

        if($request->user()->staff()) {
            $this->validate($request, [
                'form_id' => 'integer|min:1|exists:forms,id',
                'user_id' => [
                    'email',
                    'regex:/^.+@edu\.hse\.ru$/'
                ],
                'course_id' => 'required_without:category_id',
                'category_id' => 'required_without:course_id'
            ]);

            if(!$request->user()->staffHasAccessToCourse($validated['course_id'])) {
                abort(403);
            }

            if($request->has('form_id')) {
                if(!$request->user()->staffHasAccessToForm($request->input('form_id'))) {
                    abort(403);
                }

                $query->where('submissions.id', '=', $request->input('form_id'));
            }

            if($request->has('category_id') && !$request->user()->staffHasAccessToCategory($validated['category_id'])) {
                abort(403);
            }

            if($request->has('user_id')) {
                $query->where('submissions.user_id', '=', $request->input('user_id'));
            }
        }

        if($request->has('course_id')) {
            $query->join('forms', 'forms.id', '=', 'submissions.form_id')
                ->join('categories', 'categories.id', '=', 'forms.category_id')
                ->where('categories.course_id', '=', $validated['course_id'])
                ->select('submissions.*');
        }

        if($request->has('category_id')) {
            $query->join('forms', 'forms.id', '=', 'submissions.form_id')
                ->where('forms.category_id', '=', $validated['category_id']);
        }

        $data = $query->paginate($request->input('page_size', 10));

        return response()->json([
            'page' => $data->currentPage(),
            'last_page' => $data->lastPage(),
            'page_size' => $data->perPage(),
            'total_records' => $data->total(),
            'data' => $data->items()
        ]);
    }

    public function store(Request $request, FieldService $fieldService) {
        if($request->user()->staff()) {
            abort(403);
        }

        $validated = $this->validate($request, [
            'form_id' => 'required|integer|min:1|exists:forms,id',
            'answers' => 'required|array|min:1',
            'answers.*.field_id' => 'required|integer|min:1|exists:fields,id',
            'answers.*.value' => 'required'
        ]);

        /** @var Form $form */
        $form = Form::find($validated['form_id']);

        if($request->user()->student() && !$request->user()->studentHasAccessToForm($form)) {
            abort(403);
        }

        $answersCollection = collect($validated['answers']);

        foreach ($form->fields as $field) {
            $methodName = sprintf("validate%sField", ucfirst($field->type));

            $answer = $answersCollection->firstWhere('field_id', $field->id);

            if($answer === null) {
                $answer = ['field_id' => $field->id];
            }

            $fieldService->{$methodName}($answer, $field);
        }

        /** @var Submission $submission */
        $submission = Submission::create([
            'user_id' => $request->user()->id,
            'form_id' => $form->id
        ]);

        $answers = [];
        $answersWithAttachments = [];

        foreach ($validated['answers'] as $answer) {
            $res = $fieldService->createAnswer($answer, $form);

            if($res[0] instanceof Attachment) {
                $answersWithAttachments = array_merge($answersWithAttachments, [$res]);

                $res = [$res[1]];
            }

            $answers = array_merge($answers, $res);
        }

        $submission->answers()->saveMany($answers);

        foreach ($answersWithAttachments as $answersWithAttachment) {
            $answersWithAttachment[0]->attachable()->associate($answersWithAttachment[1]);

            $answersWithAttachment[0]->save();
        }

        return response()->json([
            'data' => $submission->loadMissing(['answers', 'form', 'replies'])
        ]);
    }

    public function update(Request $request, Submission $submission, FieldService $fieldService)
    {
        $statusOptions = [
            Submission::NEW,
            Submission::IN_PROGRESS,
            Submission::COMPLETED,
            Submission::REJECTED
        ];

        $validated = $this->validate($request, [
            'status' => 'in:' . implode(',', $statusOptions),
            'answers' => 'array|min:1',
            'answers.*.field_id' => 'required|integer|min:1|exists:fields,id',
            'answers.*.value' => 'required'
        ]);

        if($request->user()->student() && (!$request->user()->studentHasAccessToSubmission($submission) || $request->has('status'))) {
            abort(403);
        }

        if($request->user()->staff() && (!$request->user()->staffHasAccessToSubmission($submission) || $request->has('answers'))) {
            abort(403);
        }

        DB::beginTransaction();

        try {
            if(array_key_exists('status', $validated)) {
                $status = $validated['status'];

                if($status === Submission::NEW || $status = Submission::IN_PROGRESS) {
                    $submission->delete_at = null;
                } else {
                    $submission->delete_at = (new Carbon)->addDays($submission->form->expires_after);
                }

                $submission->status = $status;

                $submission->save();
            }

            if(array_key_exists('answers', $validated)) {
                $answers = $validated['answers'];

                foreach ($answers as $answer) {
                    $fieldService->validate($answer, $submission->form);

                    $field = $submission->form->fields->firstWhere('id', $answer['field_id']);

                    /** @var Answer $storedAnswer */
                    $storedAnswer = $submission->answers->firstWhere('field_id', $field->id);

                    if($field->type === Field::TYPE_ATTACHMENT) {
                        if($storedAnswer === null) {
                            /** @var Attachment $attachment */
                            list($attachment, $newAnswer) = $fieldService->createAttachmentAnswer($answer, $field);

                            $submission->answers()->save($newAnswer);

                            $attachment->attachable()->associate($newAnswer);

                            $attachment->save();
                        } else {
                            throw ValidationException::withMessages([
                                'submission' => "You cannot modify answer for field of type 'attachment'"
                            ]);
                        }
                    }

                    if($field->type === Field::TYPE_TEXT) {
                        if($storedAnswer === null) {
                            $storedAnswer = new Answer([
                                'field_id' => $field->id,
                                'submission_id' => $submission->id
                            ]);
                        }

                        $storedAnswer->value = $answer['value'];

                        $storedAnswer->save();
                    }

                    if($field->type === Field::TYPE_SELECT) {
                        $submission->answers()->where('field_id', $field->id)->delete();

                        $submission->answers()->saveMany($fieldService->createSelectAnswer($answer, $field));
                    }
                }
            }
        } catch (Exception $e) {
            DB::rollBack();

            throw $e;
        }

        DB::commit();

        return response()->json([
            'data' => $submission->load(['answers', 'form'])
        ]);
    }

    public function show(Request $request, Submission $submission)
    {
        if($request->user()->staff() && !$request->user()->staffHasAccessToSubmission($submission)) {
            abort(403);
        }

        if($request->user()->student() && !$request->user()->studentHasAccessToSubmission($submission)) {
            abort(403);
        }

        return response()->json([
            'data' => $submission
        ]);
    }

    public function destroy(Request $request, Submission $submission)
    {
        if($request->user()->staff() && !$request->user()->staffHasAccessToSubmission($submission)) {
            abort(403);
        }

        if($request->user()->student() && !$request->user()->studentHasAccessToSubmission($submission)) {
            abort(403);
        }

        if($submission->status !== Submission::NEW) {
            throw ValidationException::withMessages([
                'submission' => "You cannot delete submission with status \'{$submission->status}\'"
            ]);
        }

        DB::beginTransaction();

        try {
            foreach ($submission->answers as $answer) {
                if($answer->attachment !== null) {
                    $this->deleteAttachment($answer->attachment);
                }

                $answer->delete();
            }

            foreach ($submission->replies as $reply) {
                if($reply->attachment !== null) {
                    $this->deleteAttachment($reply->attachment);
                }

                $reply->delete();
            }

            $submission->delete();
        } catch (Exception $e) {
            DB::rollBack();

            throw $e;
        }

        DB::commit();

        return response()->noContent();
    }

    private function deleteAttachment(Attachment $attachment)
    {
        Storage::disk()->delete($attachment->path);

        $attachment->delete();
    }
}
