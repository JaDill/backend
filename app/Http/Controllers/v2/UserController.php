<?php

namespace App\Http\Controllers\v2;

use App\Models\UserSettings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function me(Request $request)
    {
        if($request->user() === null) {
            return abort(401);
        }

        $settings = UserSettings::find($request->user()->id);

        return response([
            'data' => [
                'user' => $request->user()->toArray(),
                'settings' => $settings,
            ]
        ]);
    }

    public function updateSettings(Request $request)
    {
        $this->validate($request, [
            'default_course' => 'required|integer|min:1|exists:courses,id'
        ]);

        if($request->user() === null) {
            return abort(401);
        }

        if(($settings = UserSettings::find($request->user()->id)) === null) {
            $settings = UserSettings::create([
                'user_id' => $request->user()->id,
                'default_course' => $request->input('default_course')
            ]);
        } else {
            $settings->default_course = $request->input('default_course');

            $settings->save();
        }

        return response()->json([
            'data' => $settings
        ]);
    }
}
