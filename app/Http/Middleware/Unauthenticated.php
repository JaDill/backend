<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Unauthenticated
{
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            return abort(401);
        }

        return $next($request);
    }
}
