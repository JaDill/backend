<?php

namespace App\Jobs;

use App\Models\Attachment;
use App\Models\Submission;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DeleteExpiredSubmissions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle()
    {
        Submission::query()
            ->without(['form'])
            ->whereDate('delete_at', '<=', (new Carbon)->toDateString())
            ->chunk(100, function(Collection $submissions) {
                $submissions->each($this->deleteSubmissions());
            });
    }

    private function deleteSubmissions() : callable
    {
        return function (Submission $submission) {
            DB::beginTransaction();

            try {
                foreach ($submission->answers as $answer) {
                    if($answer->attachment !== null) {
                        $this->deleteAttachment($answer->attachment);
                    }

                    $answer->delete();
                }

                foreach ($submission->replies as $reply) {
                    if($reply->attachment !== null) {
                        $this->deleteAttachment($reply->attachment);
                    }

                    $reply->delete();
                }

                $submission->delete();
            } catch (Exception $e) {
                DB::rollBack();

                throw $e;
            }

            DB::commit();
        };
    }

    private function deleteAttachment(Attachment $attachment)
    {
        Storage::disk()->delete($attachment->path);

        $attachment->delete();
    }
}
