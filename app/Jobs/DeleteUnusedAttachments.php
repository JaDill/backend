<?php

namespace App\Jobs;

use App\Models\Attachment;
use App\Models\Submission;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DeleteUnusedAttachments implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle()
    {
        Attachment::query()
            ->where(function(Builder $query) {
                $query->whereNull('attachable_id')
                    ->orWhereNull('attachable_type');
            })
            ->where('created_at', '<', (new Carbon)->subHour())
            ->chunk(100, function (Collection $attachments) {
                $attachments->each($this->deleteAttachments());
            });
    }

    private function deleteAttachments() : callable
    {
        return function (Attachment $attachment) {
            Storage::disk()->delete($attachment->path);

            $attachment->delete();
        };
    }
}
