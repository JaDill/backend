<?php

namespace App\Listeners;

use App\Events\CategoryCreating;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class HandleCategoryCreation
{
    /**
     * Handle the event.
     *
     * @param  CategoryCreating  $event
     * @return void
     */
    public function handle(CategoryCreating $event)
    {
        $event->category->order = (DB::table('categories')->max('order') ?? 0) + 1;
    }
}
