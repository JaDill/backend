<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccountConfirmationCode extends Mailable implements ShouldQueue
{
    public $code;

    /**
     * Create a new message instance.
     *
     * @param int $code
     */
    public function __construct(int $code)
    {
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.account.confirm')
            ->subject("Авторизация в \"Едином окне\"");
    }
}
