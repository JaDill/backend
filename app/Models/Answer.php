<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $with = [
        'attachment'
    ];

    protected $fillable = [
        'value', 'field_id', 'submission_id'
    ];

    public function field() {
        return $this->belongsTo(Field::class);
    }

    public function submission() {
        return $this->belongsTo(Submission::class);
    }

    public function attachment() {
        return $this->morphOne(Attachment::class, 'attachable');
    }

    public function getValueAttribute($value)
    {
        if(!isset($this->field_id) || ($field = Field::find($this->field_id)) === null || !$field->personal) {
            return $value;
        }

        return decrypt($value, false);
    }

    public function setValueAttribute($value)
    {
        if(!isset($this->field_id) || ($field = Field::find($this->field_id)) === null || !$field->personal) {
            $this->attributes['value'] = $value;

            return;
        }

        $this->attributes['value'] = encrypt($value, false);
    }
}
