<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attachment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'path', 'size', 'attachable_id', 'attachable_type', 'uploaded_by'
    ];

    protected $hidden = [
        'path', 'attachable_id', 'attachable_type', 'uploaded_by'
    ];

    public function attachable() {
        return $this->morphTo();
    }
}
