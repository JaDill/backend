<?php

namespace App\Models;

use App\Events\CategoryCreating;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property Collection forms
 * @property Course course
 */
class Category extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'description', 'restricted', 'order', 'course_id'
    ];

    protected $casts = [
        'order' => 'float'
    ];

    protected $hidden = [
        'restricted'
    ];

    protected $dispatchesEvents = [
        'creating' => CategoryCreating::class
    ];

    public function forms() {
        return $this->hasMany(Form::class);
    }

    public function course() {
        return $this->belongsTo(Course::class);
    }
}
