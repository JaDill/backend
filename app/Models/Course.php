<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property Collection $categories
 * @property Collection $staff
 */
class Course extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'group_regex'
    ];

    protected $hidden = [
        'updated_at', 'created_at', 'deleted_at', 'group_regex'
    ];

    public function categories() {
        return $this->hasMany(Category::class);
    }
}