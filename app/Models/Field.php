<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property Collection categories
 */
class Field extends Model
{
    use SoftDeletes; // do we need to?

    const TYPE_TEXT = 'text';
    const TYPE_SELECT = 'select';
    const TYPE_ATTACHMENT = 'attachment';

    protected $fillable = [
        'title', 'description', 'type', 'required', 'personal', 'meta', 'form_id', 'order'
    ];

    protected $casts = [
        'meta' => 'array',
        'required' => 'boolean',
        'personal' => 'boolean'
    ];

    public function form() {
        return $this->belongsTo(Form::class);
    }
}