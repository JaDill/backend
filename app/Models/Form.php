<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/** @property Collection $fields */
class Form extends Model
{
    use SoftDeletes;

    protected $with = [
        'fields'
    ];

    protected $attributes = [
        'restricted' => false,
        'expires_after' => 30
    ];

    protected $fillable = [
        'title', 'description', 'restricted', 'category_id', 'expires_after'
    ];

    protected $hidden = [
        'restricted', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function fields() {
        return $this->hasMany(Field::class);
    }

    public function submissions() {
        return $this->hasMany(Submission::class);
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }
}
