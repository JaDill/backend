<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property Submission $submission
 * @property Collection $attachments
 */
class Reply extends Model
{
    protected $fillable = [
        'reply', 'submission_id'
    ];

    protected $with = [
        'attachment'
    ];

    public function submission() {
        return $this->belongsTo(Submission::class);
    }

    public function attachment() {
        return $this->morphOne(Attachment::class, 'attachable');
    }
}