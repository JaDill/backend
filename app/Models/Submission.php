<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/** @property Form $form */
class Submission extends Model
{
    const NEW = 'new';
    const IN_PROGRESS = 'in_progress';
    const COMPLETED = 'completed';
    const REJECTED = 'rejected';

    protected $with = [
        'form', 'answers', 'replies'
    ];

    protected $attributes = [
        'status' => self::NEW,
        'delete_at' => null
    ];

    protected $fillable = [
        'form_id', 'user_id', 'status'
    ];

    public function form() {
        return $this->belongsTo(Form::class);
    }

    public function answers() {
        return $this->hasMany(Answer::class);
    }

    public function replies() {
        return $this->hasMany(Reply::class);
    }
}
