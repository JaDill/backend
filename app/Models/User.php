<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class User implements Authenticatable
{
    const STUDENT = 'student';
    const STAFF = 'staff';

    public $id;

    public $externalId = null;

    public $uns = null;

    public $type;

    public $fio = null;

    public $info = null;

    public function __construct(array $data)
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }

    public function group(): ?string
    {
        if ($this->staff()) {
            return null;
        }

        $match = [];

        if (preg_match('/^.+\s(.+)-\d{4}$/u', $this->info, $match) == 1) {
            return $match[1];
        }

        return null;
    }

    public function student(): bool
    {
        return $this->type === self::STUDENT;
    }

    public function staff(): bool
    {
        return $this->type === self::STAFF;
    }

    public function staffCoursesQuery(): Builder
    {
        return Course::query()
            ->join('course_staff', function (JoinClause $clause) {
                $clause->on('course_staff.course_id', '=', 'courses.id')
                    ->where('course_staff.user_id', '=', $this->id);
            })
            ->select('courses.*');
    }

    public function studentCourse(): ?Course
    {
        /** @var Course|null $course */
        $course = Course::query()
            ->whereRaw('? ~ "courses"."group_regex"', [$this->group()])
            ->first();

        return $course;
    }

    public function detectDefaultCourse(): ?Course
    {
        if ($this->student()) {
            return $this->studentCourse();
        }

        /** @var Course|null $course */
        $course = $this->staffCoursesQuery()->first();

        return $course;
    }

    public function studentHasAccessToCategory(Category $category): bool
    {
        $studentCourse = $this->studentCourse();

        return !($category->restricted && ($studentCourse === null || $category->course_id !== $studentCourse->id));
    }

    public function studentHasAccessToForm(Form $form): bool
    {
        $studentCourse = $this->studentCourse();

        if (!$form->restricted) {
            return true;
        }

        if ($studentCourse === null) {
            return false;
        }

        return DB::table('forms')
                ->join('categories', 'categories.id', '=', 'forms.category_id')
                ->where('forms.id', '=', $form->id)
                ->where('categories.course_id', '=', $studentCourse->id)
                ->count() === 1;
    }

    public function studentHasAccessToSubmission(Submission $submission): bool
    {
        return $submission->user_id === $this->id;
    }

    public function studentHasAccessToReply($reply): bool
    {
        if ($reply instanceof Reply) {
            $reply = $reply->id;
        }

        return DB::table('replies')
                ->join('submissions', 'submissions.id', '=', 'replies.submission_id')
                ->where('submissions.user_id', '=', $this->id)
                ->where('replies.id', '=', $reply->id)
                ->count('replies.id') === 1;
    }

    public function studentHasAccessToAttachment(Attachment $attachment): bool
    {
        if ($attachment->attachable_type === Answer::class) {
            return $attachment->attachable->user_id === $this->id;
        }

        if ($attachment->attachable_type === Reply::class) {
            return $this->studentHasAccessToReply($attachment->attachable_id);
        }
    }

    public function staffHasAccessToCourse($course): bool
    {
        if ($course instanceof Course) {
            $course = $course->id;
        }

        return DB::table('course_staff')
            ->where('user_id', $this->id)
            ->where('course_id', $course)
            ->exists();
    }

    public function staffHasAccessToCategory($category): bool
    {
        if ($category instanceof Category) {
            $category = $category->id;
        }

        return Category::query()
                ->withTrashed()
                ->join('course_staff', 'course_staff.course_id', '=', 'categories.course_id')
                ->where('categories.id', $category)
                ->where('course_staff.user_id', '=', $this->id)
                ->count('categories.id') === 1;
    }

    public function staffHasAccessToForm($form): bool
    {
        if ($form instanceof Form) {
            $form = $form->id;
        }

        return Form::query()
                ->withTrashed()
                ->join('categories', 'categories.id', '=', 'forms.category_id')
                ->join('course_staff', 'course_staff.course_id', '=', 'categories.course_id')
                ->where('forms.id', $form)
                ->where('course_staff.user_id', '=', $this->id)
                ->count('forms.id') === 1;
    }

    public function staffHasAccessToField($field): bool
    {
        if ($field instanceof Field) {
            $field = $field->id;
        }

        return Field::query()
                ->withTrashed()
                ->join('forms', 'forms.id', '=', 'fields.form_id')
                ->join('categories', 'categories.id', '=', 'forms.category_id')
                ->join('course_staff', 'course_staff.course_id', '=', 'categories.course_id')
                ->where('fields.id', $field)
                ->where('course_staff.user_id', '=', $this->id)
                ->count('fields.id') === 1;
    }

    public function staffHasAccessToSubmission($submission): bool
    {
        if ($submission instanceof Submission) {
            $submission = $submission->id;
        }

        return Submission::query()
                ->join('forms', 'forms.id', '=', 'submissions.form_id')
                ->join('categories', 'categories.id', '=', 'forms.category_id')
                ->join('course_staff', 'course_staff.course_id', '=', 'categories.course_id')
                ->where('submissions.id', $submission)
                ->where('course_staff.user_id', '=', $this->id)
                ->count('submissions.id') === 1;
    }

    public function staffHasAccessToAnswer($answer): bool
    {
        if ($answer instanceof Answer) {
            $answer = $answer->id;
        }

        return Answer::query()
                ->join('submissions', 'submissions.id', '=', 'replies.submission_id')
                ->join('forms', 'forms.id', '=', 'submissions.form_id')
                ->join('categories', 'categories.id', '=', 'forms.category_id')
                ->join('course_staff', 'course_staff.course_id', '=', 'categories.course_id')
                ->where('answers.id', $answer)
                ->where('course_staff.user_id', '=', $this->id)
                ->count('answers.id') === 1;
    }

    public function staffHasAccessToReply($reply): bool
    {
        if ($reply instanceof Reply) {
            $reply = $reply->id;
        }

        return Reply::query()
                ->join('submissions', 'submissions.id', '=', 'replies.submission_id')
                ->join('forms', 'forms.id', '=', 'submissions.form_id')
                ->join('categories', 'categories.id', '=', 'forms.category_id')
                ->join('course_staff', 'course_staff.course_id', '=', 'categories.course_id')
                ->where('replies.id', $reply)
                ->where('course_staff.user_id', '=', $this->id)
                ->count('replies.id') === 1;
    }

    public function staffHasAccessToAttachment(Attachment $attachment): bool
    {
        if ($attachment->attachable_type === Reply::class) {
            return $this->staffHasAccessToReply($attachment->attachable_id);
        }

        if ($attachment->attachable_type === Answer::class) {
            return $this->staffHasAccessToAnswer($attachment->attachable_id);
        }

        return false;
    }

    public static function createStudent(?array $data): ?self
    {
        if ($data === null) {
            return null;
        }

        $data['externalId'] = $data['id'];

        $data['id'] = $data['email'];

        unset($data['email']);

        return new self($data);
    }

    public static function createStaff(string $email): self
    {
        $data = [
            'type' => 'staff',
            'id' => $email
        ];

        return new self($data);
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'uns' => $this->uns,
            'externalId' => $this->externalId,
            'type' => $this->type,
            'fio' => $this->fio,
            'info' => $this->info
        ];
    }

    public function getAuthIdentifierName()
    {
        return 'id';
    }

    public function getAuthIdentifier()
    {
        return $this->{$this->getAuthIdentifierName()};
    }

    public function getAuthPassword()
    {
        return '';
    }

    public function getRememberToken()
    {
        return '';
    }

    public function setRememberToken($value)
    {
        return;
    }

    public function getRememberTokenName()
    {
        return '';
    }

    public function __get($name)
    {
        if ($name === 'email') {
            return $this->id;
        }
    }
}
