<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property Submission $submission
 * @property Collection $attachments
 */
class UserSettings extends Model
{
    protected $primaryKey = 'user_id';

    public $incrementing = false;

    protected $fillable = ['user_id', 'default_course'];

    protected $hidden = ['user_id'];

    public $timestamps = false;

    public function defaultCourse() {
        return $this->belongsTo(Course::class, 'default_course');
    }
}