<?php

namespace App\Providers;

use App\Extensions\Auth\PasetoAuthGuard;
use App\Extensions\Auth\RuzUserProvider;
use App\Models\Attachment;
use App\Models\Building;
use App\Models\Category;
use App\Models\Course;
use App\Models\Form;
use App\Models\Group;
use App\Models\Message;
use App\Models\Submission;
use App\Models\User;
use App\Policies\AttachmentPolicy;
use App\Policies\BuildingPolicy;
use App\Policies\CategoryPolicy;
use App\Policies\CoursePolicy;
use App\Policies\FormPolicy;
use App\Policies\GroupPolicy;
use App\Policies\MessagePolicy;
use App\Policies\SubmissionPolicy;
use App\Policies\UserPolicy;
use App\Services\RuzApiService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::extend('paseto', function ($app, $name, array $config) {
            $guard = new PasetoAuthGuard(Auth::createUserProvider($config['provider']), request());

            $this->app->refresh('request', $guard, 'setRequest');

            return $guard;
        });

        Auth::provider('ruz', function ($app, array $config) {
            return new RuzUserProvider($app->make(RuzApiService::class));
        });
    }

    public function register()
    {
        parent::register();

        $this->app->bind(RuzUserProvider::class);
    }
}
