<?php

namespace App\Providers;

use App\Services\EmailConfirmation;
use App\Services\HackCacheEmailConfirmation;
use Illuminate\Support\ServiceProvider;

class EmailConfirmationServiceProvider extends ServiceProvider {
    public function register() {
        $this->app->bind(EmailConfirmation::class, HackCacheEmailConfirmation::class);
    }
}