<?php

namespace App\Providers;

use App\Services\RuzApiService;
use Illuminate\Support\ServiceProvider;

class RuzApiServiceProvider extends ServiceProvider {
    public function register() {
        $this->app->bind(RuzApiService::class);
    }
}