<?php
namespace App\Services;

use App\Mail\AccountConfirmationCode;
use App\Models\User;
use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Illuminate\Contracts\Mail\Mailer;

class CacheEmailConfirmation implements EmailConfirmation {
    private const CACHE_PREFIX = 'auth';
    private const CACHE_LIFETIME = 60 * 24;

    private $cache;

    private $mailer;

    public function __construct(CacheRepository $cache, Mailer $mailer) {
        $this->cache = $cache;

        $this->mailer = $mailer;
    }

    private function generateCode() : int {
        try {
            return random_int(1000, 9999);
        } catch (\Exception $e) {
            return rand(1000, 9999);
        }
    }

    /**
     * @param User|string $user
     * @return string
     */
    private function getCacheKey($user) : string {
        return self::CACHE_PREFIX . ":" . ($user instanceof User ? $user->id : $user);
    }

    /**
     * @param User|string $user
     */
    public function send($user) : void {
        $code = $this->generateCode();

        $this->cache->put($this->getCacheKey($user), $code, self::CACHE_LIFETIME);

        $this->mailer->to($user)->send(new AccountConfirmationCode($code));
    }

    /**
     * @param User|string $user
     * @return bool
     */
    public function pending($user) : bool {
        return $this->cache->has($this->getCacheKey($user));
    }

    /**
     * @param User|string $user
     * @param int $code
     * @return bool
     */
    public function check($user, $code) : bool {
        $key = $this->getCacheKey($user);

        $value = $this->cache->get($key);

        if($value === null || $value != $code) {
            return false;
        }

        $this->cache->forget($key);

        return true;
    }
}