<?php


namespace App\Services;


use App\Models\Category;
use App\Models\Course;
use App\Models\Field;
use App\Models\Form;
use Illuminate\Support\Facades\DB;
use InvalidArgumentException;
use LogicException;

class CourseService
{
    private $categories = [
        'spravki' => [
            'title' => 'Заказ справок',
            'description' => 'Формы для заказа справок',
            'restricted' => true
        ],
        'receipt' => [
            'title' => 'Загрузка квитанции об оплате обучения',
            'description' => null,
            'restricted' => true
        ],
        'booking' => [
            'title' => 'Бронирование аудитории',
            'description' => 'Забронировать аудиторию в здании направления',
            'restricted' => false
        ]
    ];

    public function createBaseCategories(Course $course)
    {
        $categories = [];

        foreach ($this->categories as $key => $category) {
            $categories[$key] = Category::create(array_merge(['course_id' => $course->id], $category));
        }

        return $categories;
    }

    public function createBaseBookingForm(Category $category) {
        $form = Form::create([
            'title' => 'Бронирование аудитории',
            'description' => 'Форма для бронирование аудитории',
            'restricted' => $category->restricted,
            'category_id' => $category->id,
        ]);

        $fields = [
            [
                'title' => 'Период использования',
                'description' => 'С _ числа до _ числа',
                'type' => 'text',
                'required' => true,
                'personal' => false,
                'order' => 0.1
            ],
            [
                'title' => 'Время использования',
                'description' => 'С _ по _ ч',
                'type' => 'text',
                'required' => true,
                'personal' => false,
                'order' => 0.2
            ],
            [
                'title' => 'ФИО ответственного',
                'type' => 'text',
                'required' => true,
                'personal' => false,
                'order' => 0.3
            ],
            [
                'title' => 'Номер телефона ответственного',
                'type' => 'text',
                'required' => true,
                'personal' => false,
                'order' => 0.4
            ],
            [
                'title' => 'Дополнительная информация',
                'description' => 'Кол-во посетителей, необходимость в проекторе и т.п.',
                'type' => 'text',
                'required' => false,
                'personal' => false,
                'order' => 0.5
            ]
        ];

        foreach ($fields as $field) {
            Field::create(array_merge($field, ['form_id' => $form->id]));
        }
    }

    public function createBaseReceiptForm(Category $category) {
        $form = Form::create([
            'title' => 'Загрузка квитанции',
            'description' => 'Форма для загрузки квитанции',
            'restricted' => $category->restricted,
            'category_id' => $category->id,
        ]);

        $fields = [
            [
                'title' => 'Квитанция об оплате',
                'description' => 'Загрузите файл',
                'type' => 'attachment',
                'required' => true,
                'personal' => false,
                'order' => 0.5
            ]
        ];

        foreach ($fields as $field) {
            Field::create(array_merge($field, ['form_id' => $form->id]));
        }
    }

    public function createBaseSpravkiForm(Category $category) {
        $form = Form::create([
            'title' => 'Заказ справки',
            'description' => 'Форма для заказа справки',
            'restricted' => $category->restricted,
            'category_id' => $category->id,
        ]);

        $fields = [
            [
                'title' => 'Вид справки',
                'type' => 'select',
                'required' => true,
                'personal' => false,
                'order' => 0.1,
                'meta' => [
                    'options' => [
                        'Справка об успеваемости',
                        'Справка об обучении',
                        'Справка в органы МВД',
                        'Справка об уровне владения иностранным языком',
                        'Справка об успеваемости для банка',
                        'Счет для банка',
                        'Академическая справка',
                        'Копия лицензий и свидетельства о государственной аккредитации',
                        'Копия приказа об отчислении'
                    ],
                    'multiple' => false
                ]
            ],
            [
                'title' => 'Язык справки',
                'type' => 'select',
                'required' => true,
                'personal' => false,
                'order' => 0.3,
                'meta' => [
                    'options' => [
                        'Русский',
                        'Английский'
                    ],
                    'multiple' => false
                ]
            ],
            [
                'title' => 'Количество экземпляров',
                'type' => 'select',
                'required' => true,
                'personal' => false,
                'order' => 0.46,
                'meta' => [
                    'options' => [1, 2, 3],
                    'multiple' => false
                ]
            ],
            [
                'title' => 'Название организации, в которую предоставляется справка',
                'description' => 'Если вы заказываете несколько экземпляров справки, то перечислите все организации, в которые они будут предоставлены.',
                'type' => 'text',
                'required' => true,
                'personal' => false,
                'order' => 0.48
            ],
            [
                'title' => 'Контактная информация',
                'description' => 'Укажите свой номер телефона',
                'type' => 'text',
                'required' => true,
                'personal' => false,
                'order' => 0.49
            ],
            [
                'title' => 'Дополнительная информация',
                'description' => 'Укажите свой комментарий, если это необходимо',
                'type' => 'text',
                'required' => false,
                'personal' => false,
                'order' => 0.5
            ]
        ];

        foreach ($fields as $field) {
            Field::create(array_merge($field, ['form_id' => $form->id]));
        }
    }

    public function create(array $data, array $staffs = []) : Course
    {
        DB::beginTransaction();

        $course = Course::create($data);

        $this->addStaff($course, $staffs);

        $categories = $this->createBaseCategories($course);

        foreach ($categories as $key => $category) {
            $methodName = "createBase" . ucfirst($key) . "Form";

            $this->{$methodName}($category);
        }

        DB::commit();

        return $course;
    }

    /**
     * @param Course $course
     * @param string|string[] $staff
     */
    public function addStaff(Course $course, $staff)
    {
        if(is_string($staff)) {
            $staff = [$staff];
        }

        if(!is_array($staff)) {
            throw new InvalidArgumentException("2nd argument type must be string or array");
        }

        $records = array_map(function ($u) use ($course) {
            return ['course_id' => $course->id, 'user_id' => $u];
        }, $staff);

        DB::table('course_staff')->insert($records);
    }

    /**
     * @param Course $course
     * @param string|string[] $staff
     */
    public function removeStaff(Course $course, $staff)
    {
        if(is_string($staff)) {
            $staff = [$staff];
        }

        if(!is_array($staff)) {
            throw new InvalidArgumentException("2nd argument type must be string or array");
        }

        DB::beginTransaction();

        $c = DB::table('course_staff')
            ->where('course_id', $course->id)
            ->whereIn('user_id', $staff)
            ->delete();

        if($c !== sizeof($staff)) {
            DB::rollBack();

            throw new LogicException("2nd argument contains users which are not associated with given course");
        }

        DB::commit();
    }
}