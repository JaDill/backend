<?php
namespace App\Services;

interface EmailConfirmation
{
    public function send($user) : void;

    public function pending($user) : bool;

    public function check($user, $code) : bool;
}