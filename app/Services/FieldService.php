<?php
namespace App\Services;

use App\Models\Answer;
use App\Models\Attachment;
use App\Models\Field;
use App\Models\Form;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use InvalidArgumentException;
use LogicException;

class FieldService
{
    public function validate(array $data, Form $form)
    {
        $form->loadMissing('fields');

        if(($field = $form->fields->firstWhere('id', $data['field_id'])) === null) {
            throw ValidationException::withMessages([
                'fields' => 'Invalid field id provided'
            ]);
        }

        $type = ucfirst($field->type);

        $methodName = "validate{$type}Field";

        if(!method_exists($this, $methodName)) {
            throw new LogicException("Unknown type of field \'{$field->type}\'");
        }

        $this->{$methodName}($data, $field);
    }

    public function validateTextField(array $data, Field $field)
    {
        $validator = Validator::make($data, [
            'value' => 'string'
        ]);

        $validator->sometimes('value', 'required', function () use ($field) {
            return $field->required;
        });

        $validator->validate();
    }

    public function validateSelectField(array $data, Field $field)
    {
        $validator = Validator::make($data, [
            'value' => 'array|min:1',
            'value.*' => 'required|distinct|in:' . implode(',', $field->meta['options'])
        ]);

        $validator->sometimes('value', 'required', function () use ($field) {
            return $field->required;
        });

        $validator->sometimes('value', 'size:1', function () use ($field) {
            return !($field->meta['multiple'] ?? false);
        });

        $validator->validate();
    }

    public function validateAttachmentField(array $data, Field $field)
    {
        $validator = Validator::make($data, [
            'value' => 'integer|min:1|exists:attachments,id',
        ]);

        $validator->sometimes('value', 'required', function () use ($field) {
            return $field->required;
        });

        $validator->validate();

        $attachment = Attachment::query()->find($data['value']);

        if($attachment->attachable_id !== null || $attachment->uploaded_by !== request()->user()->id) {
            throw ValidationException::withMessages([
                'attachment' => 'You are not allowed to use this attachment'
            ]);
        }
    }

    public function createAnswer(array $data, Form $form)
    {
        $form->loadMissing('fields');

        if(($field = $form->fields->firstWhere('id', $data['field_id'])) === null) {
            throw new InvalidArgumentException("Invalid field id provided");
        }

        $type = ucfirst($field->type);

        $methodName = "create{$type}Answer";

        if(!method_exists($this, $methodName)) {
            throw new LogicException("Unknown type of field \'{$field->type}\'");
        }

        return $this->{$methodName}($data, $field);
    }

    public function createTextAnswer(array $data, Field $field) : array
    {
        return [new Answer([
            'field_id' => $field->id,
            'value' => $data['value']
        ])];
    }

    public function createSelectAnswer(array $data, Field $field) : array
    {
        $answers = [];

        foreach ($data['value'] as $value) {
            $answers[] = new Answer([
                'field_id' => $field->id,
                'value' => $value
            ]);
        }

        return $answers;
    }

    public function createAttachmentAnswer(array $data, Field $field) : array
    {
        return [Attachment::find($data['value']), new Answer([
            'field_id' => $field->id,
            'value' => null
        ])];
    }

    public function updateAnswer(array $data, Answer $answer)
    {
        $answer->loadMissing('field');

        $type = ucfirst($answer->field->type);

        $methodName = "update{$type}Answer";

        if(!method_exists($this, $methodName)) {
            throw new LogicException("Unknown type of field \'{$type}\'");
        }

        return $this->{$methodName}($data, $answer);
    }

    public function updateTextAnswer(array $data, Answer $answer)
    {
        $this->validateTextField($data, $answer->field);

        $answer->value = $data['value'];

        $answer->save();
    }
}