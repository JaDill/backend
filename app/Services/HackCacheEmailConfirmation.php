<?php
namespace App\Services;

class HackCacheEmailConfirmation extends CacheEmailConfirmation
{
    private const IGNORED = ['test@hse.ru'];

    public function check($user, $code) : bool
    {
        if(in_array($user, self::IGNORED)) {
            return true;
        }

        return parent::check($user, $code);
    }

    public function pending($user): bool
    {
        if(in_array($user, self::IGNORED)) {
            return true;
        }

        return parent::pending($user);
    }
}