<?php
namespace App\Services;

use GuzzleHttp\Client;

class RuzApiService
{
    private $client;

    const STUDENT_INFO_ENDPOINT = 'studentinfo';

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://ruz.hse.ru/api/',
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            'verify' => false
        ]);
    }

    /**
     * @param int|string $data Id or email of student
     * @return array|null Student information
     */
    public function getStudentInfo($data) : ?array
    {
        if(str_contains((string) $data, '@')) {
            $response = $this->getStudentInfoUsingEmail($data);
        } else {
            $response = $this->getStudentInfoUsingId(intval($data));
        }

        if(array_key_exists('error', $response)) {
            return null;
        }

        return $response;
    }

    private function getStudentInfoUsingId(int $id) : array
    {
        $uri = sprintf("%s/%d", self::STUDENT_INFO_ENDPOINT, $id);

        return json_decode($this->client->get($uri)->getBody(), true);
    }

    private function getStudentInfoUsingEmail(string $email) : array
    {
        $response = $this->client->get(self::STUDENT_INFO_ENDPOINT, [
            'query' => [
                'email' => $email
            ]
        ]);

        return json_decode($response->getBody(), true);
    }
}