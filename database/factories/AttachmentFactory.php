<?php

use App\Models\Attachment;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Http\UploadedFile;

/** @var Factory $factory */
$factory->define(Attachment::class, function (Faker $faker) {
    $file = UploadedFile::fake()->image('image.png');

    return [
        'name' => 'test.png',
        'path' => $file->store('attachments'),
        'size' => $file->getSize()
    ];
});
