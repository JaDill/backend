<?php

use Faker\Generator as Faker;
use App\Models\Category;
use App\Models\Course;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(Category::class, function (Faker $faker) {
    return [
        'title' => $faker->realText(70),
        'description' => $faker->realText(300),
        'course_id' => function() {
            return factory(Course::class);
        },
        'restricted' => $faker->boolean
    ];
});
