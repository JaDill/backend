<?php

use Faker\Generator as Faker;
use App\Models\Course;
use App\Models\Faculty;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(Course::class, function (Faker $faker) {
    return [
        'title' => $faker->realText(40),
        'group_regex' => $faker->lastName
    ];
});
