<?php

use App\Models\Field;
use App\Models\Form;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(Field::class, function (Faker $faker) {
    return [
        'title' => $faker->realText(70),
        'description' => $faker->realText(200),
        'form_id' => function() {
            return factory(Form::class);
        },
        'required' => true,
        'type' => Field::TYPE_TEXT,
        'order' => $faker->randomFloat(2, -10, 10)
    ];
});

$factory->state(Field::class, 'select', function () {
    return [
        'type' => Field::TYPE_SELECT,
        'meta' => [
            'options' => [
                'Значение 1',
                'Значение 2',
                'Значение 3'
            ],
            'multiple' => true
        ]
    ];
});

$factory->state(Field::class, 'attachment', function () {
    return [
        'type' => Field::TYPE_ATTACHMENT
    ];
});