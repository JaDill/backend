<?php

use Faker\Generator as Faker;
use App\Models\Form;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(Form::class, function (Faker $faker) {
    $category = factory(Category::class)->create();

    return [
        'title' => $faker->realText(70),
        'description' => $faker->realText(300),
        'category_id' => $category->id,
        'restricted' => $category->restricted ?: $faker->boolean
    ];
});
