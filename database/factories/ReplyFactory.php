<?php

use App\Models\Reply;
use App\Models\Submission;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(Reply::class, function (Faker $faker) {
    return [
        'submission_id' => function() {
            return factory(Submission::class);
        },
        'reply' => $faker->realText()
    ];
});