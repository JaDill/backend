<?php

use Faker\Generator as Faker;
use App\Models\Form;
use App\Models\User;
use App\Models\Submission;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(Submission::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(User::class)->make()->id;
        },
        'form_id' => function() {
            return factory(Form::class);
        },
        'status' => $faker->randomElement([
            Submission::NEW, Submission::COMPLETED, Submission::REJECTED, Submission::IN_PROGRESS
        ])
    ];
});
