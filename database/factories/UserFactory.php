<?php

use App\Models\User;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(User::class, function () {
    return [
        'id' => 'dvgluschenko@edu.hse.ru',
        'externalId' => '131517',
        'uns' => 'М171БПИНЖ040',
        'type' => User::STUDENT,
        'fio' => 'Глущенко Даниил Валерьевич',
        'info' => 'Бакалавриат группа БПИ174-2017'
    ];
});

$factory->state(User::class, User::STAFF, function() {
    return [
        'id' => 'test@hse.ru',
        'externalId' => null,
        'uns' => null,
        'type' => User::STAFF,
        'fio' => null,
        'info' => null
    ];
});