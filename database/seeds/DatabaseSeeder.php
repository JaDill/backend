<?php

use App\Models\Field;
use App\Services\CourseService;
use Illuminate\Database\Seeder;
use App\Models\Course;
use App\Models\Category;
use App\Models\Form;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $courseService = new CourseService();

        DB::beginTransaction();

        $courses = [
            'pi' => [
                'title' => 'Программная инженерия',
                'group_regex' => 'БПИ\d{3}'
            ],
            'pmi' => [
                'title' => 'Прикладная математика и информатика',
                'group_regex' => 'БПМИ\d{3}'
            ],
            'law' => [
                'title' => 'Юриспруденция',
                'group_regex' => 'БЮР\d{3}'
            ],
            'civilcom' => [
                'title' => 'Гражданское и коммерческое право',
                'group_regex' => 'МГП\d{3}'
            ]
        ];

        foreach ($courses as $course) {
            $courseService->create($course, ['test@hse.ru']);
        }

        DB::commit();
    }
}
