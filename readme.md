### Локальная разработка
Предполагается, что ты уже установил и настроил проект [deploy](https://gitlab.com/wingle/deploy) (из ветки local).

Баш в контейнере - `docker-compose exec backend bash`. Помни, что после остановки контейнера все изменения внутри сотрутся (кроме, конечно же, /var/www).

При первом запуске необходимо скачать зависимости, запустить миграции и еще кое-что :^)
```
composer install
php artisan migrate # Внутри контейнера есть алиас art='php artisan'
php artisan key:generate
ln -s /var/www/storage/app/public /var/www/public/storage
```