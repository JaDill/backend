<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'invalid_credentials' => [
        'title' => 'Введенные вами данные не удалось найти в системе.'
    ],
    'already_registered' => [
        'title' => 'Пользователь с данным адрес эл. почты уже завершил регистрацию в системе.',
        'description' => 'Если вы являетесь владельцем данного аккаунта, воспользуйтесь восстановлением пароля.'
    ],
    'wrong_code' => [
        'title' => 'Введен неверный код'
    ],
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'email_not_found' => [
        'title' => 'Пользователь с указанным адресом эл. почты не найден'
    ]
];
