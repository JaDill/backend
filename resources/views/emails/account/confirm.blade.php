@component('mail::layout')
@slot('header')
@component('mail::header', ['url' => '#'])
Единое окно ВШЭ
@endcomponent
@endslot

# Подтвердите адрес эл. почты

Для завершения процесса авторизации, пожалуйста, вопспользуйтесь данным кодом:

@component('mail::panel')
{{ $code }}
@endcomponent

@slot('footer')
@component('mail::footer')
Данное письмо было отправлено автоматически. Пожалуйста, не отвечайте на него.
@endcomponent
@endslot
@endcomponent