<?php

use \Illuminate\Support\Facades\Route;

Route::prefix('v2')->namespace('v2')->group(function() {
    Route::middleware('guest')->prefix('auth')->group(function () {
        Route::post('email', 'AuthController@email')->name('auth.email');
        Route::post('code', 'AuthController@code')->name('auth.code');
        Route::post('resend', 'AuthController@resend')->name('auth.resend');
    });

    Route::middleware('auth')->group(function () {
        Route::get('user/me', 'UserController@me')->name('user.me');
        Route::post('user/settings', 'UserController@updateSettings')->name('user.settings');

        Route::patch('form/{form}/restore', 'FormController@restore')->name('form.restore');

        Route::patch('category/{trashed_category}/restore', 'CategoryController@restore')->name('category.restore');

        Route::get('attachment/{attachment}/download', 'AttachmentController@download')->name('attachment.download');

        Route::resource('course', 'CourseController')->only(['index', 'show']);

        Route::post('reply', 'ReplyController@store')->name('reply.store');

        Route::apiResources([
            'form' => 'FormController',
            'category' => 'CategoryController',
            'submission' => 'SubmissionController',
            'attachment' => 'AttachmentController'
        ]);
    });
});