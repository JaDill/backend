<?php

namespace Tests\Feature;

use App\Models\Attachment;
use App\Models\Category;
use App\Models\Course;
use App\Models\Field;
use App\Models\Form;
use App\Models\User;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AttachmentTest extends TestCase
{
    private $token;

    protected function setUp()
    {
        parent::setUp();

        $this->token = auth()->login('dvgluschenko@edu.hse.ru');
    }

    public function json($method, $uri, array $data = [], array $headers = [])
    {
        $token = auth()->login('dvgluschenko@edu.hse.ru');

        $headers = array_merge(['Authorization' => $token], $headers);

        return parent::json($method, $uri, $data, $headers);
    }

    public function get($uri, array $headers = [])
    {
        $headers = array_merge(['Authorization' => $this->token], $headers);

        return parent::get($uri, $headers);
    }

    public function patch($uri, array $data = [], array $headers = [])
    {
        $headers = array_merge(['Authorization' => $this->token, 'Accept' => 'application/json'], $headers);

        return parent::patch($uri, $data, $headers);
    }

    public function post($uri, array $data = [], array $headers = [])
    {
        $headers = array_merge(['Authorization' => $this->token, 'Accept' => 'application/json'], $headers);

        return parent::post($uri, $data, $headers);
    }

    private function storeSubmission(?Form $form = null, ?array $fields = null, ?array $answers = null) : TestResponse
    {
        if($form === null) {
            $form = factory(Form::class)->create();
        }

        if($fields === null) {
            $fields = [
                Field::TYPE_TEXT => factory(Field::class)->create(['form_id' => $form->id]),
                Field::TYPE_SELECT => factory(Field::class)->state('select')->create(['form_id' => $form->id]),
                Field::TYPE_ATTACHMENT => factory(Field::class)->state('attachment')->create(['form_id' => $form->id])
            ];
        } else {
            $form->fields()->saveMany($fields);
        }

        $file = UploadedFile::fake()->image('test.png');

        $attachment = Attachment::create([
            'name' => 'image.png',
            'path' => $file->store('attachments'),
            'size' => $file->getSize(),
            'uploaded_by' => 'dvgluschenko@edu.hse.ru'
        ]);

        $data = [
            'form_id' => $form->id,
            'answers' => $answers ?? [
                    [
                        'field_id' => $fields[Field::TYPE_TEXT]->id,
                        'value' => 'kekekeek'
                    ],
                    [
                        'field_id' => $fields[Field::TYPE_SELECT]->id,
                        'value' => [
                            'Значение 1', 'Значение 2'
                        ]
                    ],
                    [
                        'field_id' => $fields[Field::TYPE_ATTACHMENT]->id,
                        'value' => $attachment->id
                    ]
                ]
        ];

        return $this->post(route('submission.store'), $data);
    }

    public function testCanUpdateAnswerAttachment()
    {
        Storage::fake();

        $this->storeSubmission();

        /** @var Attachment $attachment */
        $attachment = Attachment::first();

        $file = UploadedFile::fake()->image('test2.png');

        $response = $this->patch(route('attachment.update', ['attachment' => $attachment->id]), [
            'name' => 'kek2.png',
            'file' => $file
        ]);

        $response->assertSuccessful()
            ->assertJson([
                'data' => [
                    'id' => $attachment->id,
                    'name' => 'kek2.png'
                ]
            ]);

        Storage::disk()->assertMissing($attachment->path);
    }

    public function testCanDeleteAnswerAttachment()
    {
        Storage::fake();

        $this->storeSubmission(null, [
            Field::TYPE_TEXT => factory(Field::class)->create(),
            Field::TYPE_SELECT => factory(Field::class)->state('select')->create(),
            Field::TYPE_ATTACHMENT => factory(Field::class)->state('attachment')->create(['required' => false])
        ]);

        /** @var Attachment $attachment */
        $attachment = Attachment::first();

        $response = $this->deleteJson(route('attachment.destroy', ['attachment' => $attachment->id]));

        $response->assertStatus(204);

        Storage::disk()->assertMissing($attachment->path);
    }

    public function testCanDownloadAnswerAttachment()
    {
        Storage::fake();

        $this->storeSubmission();

        /** @var Attachment $attachment */
        $attachment = Attachment::first();

        $response = $this->get(route('attachment.download', ['attachment' => $attachment->id]));

        $response->assertSuccessful();
    }
}
