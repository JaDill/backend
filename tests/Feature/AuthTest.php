<?php

namespace Tests\Feature;

use App\Extensions\Auth\RuzUserProvider;
use App\Mail\AccountConfirmationCode;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /** @var User $student */
    private $student;

    /** @var User $staff */
    private $staff;

    protected function setUp()
    {
        parent::setUp();

        $provider = $this->app->make(RuzUserProvider::class);

        $this->student = $provider->retrieveById('dvgluschenko@edu.hse.ru');

        $this->staff = $provider->retrieveById('kek@hse.ru');
    }

    public function testCanSendCodeToHseAddress() {
        Mail::fake();

        $this->postJson(route('auth.email'), ['email' => $this->student->id])
            ->assertStatus(204);

        Mail::assertQueued(AccountConfirmationCode::class);
    }

    public function testCanNotSendCodeToNonHseAddress() {
        Mail::fake();

        $this->postJson(route('auth.email'), ['email' => 'test@test.ru'])
            ->assertStatus(422);

        Mail::assertNothingQueued();
    }

    public function testCanNotSendCodeToFakeHseStudentAddress() {
        Mail::fake();

        $this->postJson(route('auth.email'), ['email' => 'killmeplease@edu.hse.ru'])
            ->assertStatus(422);

        Mail::assertNothingQueued();
    }

    public function testCanVerifyCode() {
        Mail::fake();

        $this->postJson(route('auth.email'), ['email' => $this->staff->id])
            ->assertStatus(204);

        Mail::assertQueued(AccountConfirmationCode::class, function(AccountConfirmationCode $mail) {
            $response = $this->postJson(route('auth.code'), [
                'email' => $this->staff->id,
                'code' => $mail->code
            ])->assertStatus(200)->assertJson([
                'data' => [
                    'user' => [
                        'id' => $this->staff->id
                    ]
                ]
            ]);

            $response->assertJsonStructure([
                'data' => [
                    'user' => [
                        'id',
                        'type'
                    ],
                    'token'
                ]
            ]);

            return true;
        });
    }

    public function testCanNotVerifyWrongCode() {
        Mail::fake();

        $this->postJson(route('auth.email'), ['email' => $this->staff->id])
            ->assertStatus(204);

        Mail::assertQueued(AccountConfirmationCode::class, function(AccountConfirmationCode $mail) {
            $this->postJson(route('auth.code'), [
                'email' => $this->staff->id,
                'code' => ($mail->code + 1) % 10000
            ])->assertStatus(422);

            return true;
        });
    }

    public function testCanNotVerifyCodeWithoutSendingEmail() {
        Mail::fake();

        $this->postJson(route('auth.code'), [
            'email' => $this->staff->id,
            'code' => 42
        ])->assertStatus(422);
    }

    public function testCanResendCode() {
        $data = ['email' => $this->student->id];

        $this->postJson(route('auth.email'), $data);

        $this->postJson(route('auth.resend'), $data)
            ->assertStatus(204);
    }

    public function testCanNotResendCodeWithoutSendingItFirst() {
        $this->postJson(route('auth.resend'), ['email' => $this->student->id])
            ->assertStatus(422);
    }

    public function testAmAuthorized() {
        Mail::fake();

        $this->postJson(route('auth.email'), ['email' => $this->staff->id]);

        Mail::assertQueued(AccountConfirmationCode::class, function(AccountConfirmationCode $mail) {
            $response = $this->postJson(route('auth.code'), [
                'email' => $this->staff->id,
                'code' => $mail->code
            ]);

            $this->postJson(route('auth.email'), [
                'email' => $this->staff->id
            ], [
                'Authorization' => 'Bearer ' . $response->json('data.token')
            ])->assertStatus(401);

            return true;
        });
    }
}
