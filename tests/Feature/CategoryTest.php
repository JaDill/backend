<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Course;
use App\Models\Field;
use App\Models\Form;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    public function json($method, $uri, array $data = [], array $headers = [])
    {
        $token = auth()->login('dvgluschenko@edu.hse.ru');

        $headers = array_merge(['Authorization' => $token], $headers);

        return parent::json($method, $uri, $data, $headers);
    }

    public function testCanSimpleIndex() {
        $course = factory(Course::class)->create();

        factory(Category::class, 13)->create([
            'course_id' => $course->id,
            'restricted' => false
        ]);

        factory(Category::class, 3)->create([
            'course_id' => $course->id,
            'restricted' => true
        ]);

        factory(Category::class, 3)->create();

        $this->getJson(route('category.index') . '?course_id=' . $course->id)
            ->assertSuccessful()
            ->assertJsonStructure([
                'page',
                'last_page',
                'page_size',
                'total_records',
                'data' => [
                    '*' => ['id', 'title', 'description', 'course_id', 'order']
                ]
            ])->assertJson(['page' => 1, 'total_records' => 13]);
    }

    public function testCanRead() {
        $course = factory(Course::class)->create(['group_regex' => 'БПИ\d{3}']);
        $category = factory(Category::class)->create(['course_id' => $course->id]);

        $this->getJson(route('category.show', ['category' => $category->id]))
            ->assertSuccessful()
            ->assertJson([
                'data' => [
                    'id' => $category->id,
                    'title' => $category->title
                ]
            ]);
    }

    public function testCanDeleteAsStaff() {
        $token = auth()->login('test@hse.ru');

        $course = factory(Course::class)->create();

        DB::table('course_staff')->insert([['course_id' => $course->id, 'user_id' => 'test@hse.ru']]);

        $category = factory(Category::class)->create(['course_id' => $course->id]);

        /** @var Collection $forms */
        $forms = factory(Form::class, 4)->create(['category_id' => $category->id])->each(function (Form $form) {
            $form->fields()->save(factory(Field::class)->make(['form_id' => null]));
        });

        $response = $this->deleteJson(
            route('category.destroy', ['category' => $category->id]),
            [],
            ['Authorization' => 'Bearer ' . $token]
        );

        $response->assertStatus(204);

        $category->refresh();

        $forms->each(function (Form $form) {
            $form->refresh();
        });

        $this->assertTrue($category->deleted_at !== null);

        $this->assertTrue($forms->every(function (Form $form) {
            return $form->deleted_at !== null && $form->fields()->withTrashed()->get()->every('deleted_at', '!=', null);
        }));
    }

    public function testCanRestoreAsStaff() {
        $token = auth()->login('test@hse.ru');

        $course = factory(Course::class)->create();

        DB::table('course_staff')->insert([['course_id' => $course->id, 'user_id' => 'test@hse.ru']]);

        $category = factory(Category::class)->create(['course_id' => $course->id, 'deleted_at' => now()]);

        $response = $this->patchJson(
            route('category.restore', ['trashed_category' => $category->id]),
            [],
            ['Authorization' => 'Bearer ' . $token]
        );

        $response->assertStatus(204);

        $category->refresh();

        $this->assertTrue($category->deleted_at === null);
    }
}
