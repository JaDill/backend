<?php

namespace Tests\Feature;

use App\Models\Course;
use Tests\TestCase;

class CourseTest extends TestCase
{
    public function testCanSimpleIndex() {
        factory(Course::class, 13)->create(['group_regex' => 'БПИ\d{3}']);

        $token = auth()->login('dvgluschenko@edu.hse.ru');

        $this->getJson(route('course.index'), [
            'Authorization' => 'Bearer ' . $token
        ])->assertSuccessful()->assertJsonStructure([
            'page',
            'last_page',
            'page_size',
            'total_records',
            'data' => [
                '*' => ['id', 'title']
            ]
        ])->assertJson(['page' => 1, 'total_records' => 13]);
    }

    public function testCanQueryIndex() {
        factory(Course::class, 24)->create();

        $id = factory(Course::class, 3)->create(['title' => 'kekkekekekekekeke'])->pluck('id')->toArray();

        $token = auth()->login('dvgluschenko@edu.hse.ru');

        $actual = $this->getJson(route('course.index', [
            'query' => 'kek'
        ]), [
            'Authorization' => 'Bearer ' . $token
        ])->assertSuccessful()->json('data.*.id');

        $this->assertEquals($id, $actual);
    }

    public function testCanRead() {
        $course = factory(Course::class)->create();

        $token = auth()->login('dvgluschenko@edu.hse.ru');

        $this->getJson(route('course.show', ['course' => $course->id]), ['Authorization' => $token])
            ->assertSuccessful()
            ->assertJson([
                'data' => [
                    'id' => $course->id,
                    'title' => $course->title
                ]
            ]);
    }
}