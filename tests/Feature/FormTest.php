<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Course;
use App\Models\Field;
use App\Models\Form;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class FormTest extends TestCase
{
    public function json($method, $uri, array $data = [], array $headers = [])
    {
        $token = auth()->login('dvgluschenko@edu.hse.ru');

        $headers = array_merge(['Authorization' => $token], $headers);

        return parent::json($method, $uri, $data, $headers);
    }

    public function testCanSimpleIndex() {
        $course = factory(Course::class)->create(['group_regex' => 'БПИ\d{3}']);
        $category = factory(Category::class)->create(['course_id' => $course->id]);
        $forms = factory(Form::class, 13)->create(['category_id' => $category->id]);

        $this->getJson(route('form.index') . '?category_id=' . $forms[0]->category_id)
            ->assertSuccessful()
            ->assertJsonStructure([
                'page',
                'last_page',
                'page_size',
                'total_records',
                'data' => [
                    '*' => ['id', 'title', 'description', 'category_id']
                ]
            ])->assertJson(['page' => 1]);
    }

    public function testCanRead() {
        $course = factory(Course::class)->create(['group_regex' => 'БПИ\d{3}']);
        $category = factory(Category::class)->create(['course_id' => $course->id]);
        $form = factory(Form::class)->create(['category_id' => $category->id]);

        $this->getJson(route('form.show', ['form' => $form->id]))
            ->assertSuccessful()
            ->assertJson([
                'data' => [
                    'id' => $form->id,
                    'title' => $form->title
                ]
            ]);
    }

    public function testCanStoreFormAsStaff() {
        $token = auth()->login('test@hse.ru');

        $course = factory(Course::class)->create();

        DB::table('course_staff')->insert([['course_id' => $course->id, 'user_id' => 'test@hse.ru']]);

        $category = factory(Category::class)->create(['course_id' => $course->id]);

        $data = [
            'title' => 'testtest',
            'restricted' => $category->restricted,
            'category_id' => $category->id,
            'expires_after' => 35,
            'fields' => [
                [
                    'title' => 'kekke',
                    'type' => Field::TYPE_TEXT,
                    'required' => true,
                    'personal' => true,
                    'order' => -0.32
                ],
                [
                    'title' => 'kekkedsad',
                    'type' => Field::TYPE_SELECT,
                    'required' => true,
                    'personal' => false,
                    'order' => -0.31,
                    'meta' => [
                        'multiple' => false,
                        'options' => [
                            'kek1', 'kek2'
                        ]
                    ]
                ],
                [
                    'title' => 'kekke',
                    'type' => Field::TYPE_ATTACHMENT,
                    'required' => true,
                    'personal' => false,
                    'order' => -5.32
                ]
            ]
        ];

        $response = $this->postJson(route('form.store'), $data, ['Authorization' => 'Bearer ' . $token]);

        $response->assertSuccessful();
    }

    // TODO store, update, destroy, restore
}
