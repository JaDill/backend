<?php

namespace Tests\Feature;

use App\Models\Attachment;
use App\Models\Category;
use App\Models\Course;
use App\Models\Field;
use App\Models\Form;
use App\Models\Reply;
use App\Models\Submission;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class SubmissionTest extends TestCase
{
    private $token;

    protected function setUp()
    {
        parent::setUp();

        $this->token = auth()->login('dvgluschenko@edu.hse.ru');
    }

    public function json($method, $uri, array $data = [], array $headers = [])
    {
        $headers = array_merge(['Authorization' => $this->token], $headers);

        return parent::json($method, $uri, $data, $headers);
    }

    public function post($uri, array $data = [], array $headers = [])
    {
        $headers = array_merge(['Authorization' => $this->token, 'Accept' => 'application/json'], $headers);

        return parent::post($uri, $data, $headers);
    }

    private function storeSubmission(?Form $form = null, ?array $fields = null, ?array $answers = null) : TestResponse
    {
        if($form === null) {
            $course = factory(Course::class)->create(['group_regex' => 'БПИ\d{3}']);
            $category = factory(Category::class)->create(['course_id' => $course->id]);
            $form = factory(Form::class)->create(['category_id' => $category->id]);
        }

        if($fields === null) {
            $fields = [
                Field::TYPE_TEXT => factory(Field::class)->create(['form_id' => $form->id]),
                Field::TYPE_SELECT => factory(Field::class)->state('select')->create(['form_id' => $form->id]),
                Field::TYPE_ATTACHMENT => factory(Field::class)->state('attachment')->create(['form_id' => $form->id])
            ];
        } else {
            $form->fields()->saveMany($fields);
        }

        Storage::fake();

        $file = UploadedFile::fake()->image('test.png');

        $attachment = Attachment::create([
            'name' => 'image.png',
            'path' => $file->store('attachments'),
            'size' => $file->getSize(),
            'uploaded_by' => 'dvgluschenko@edu.hse.ru'
        ]);

        $data = [
            'form_id' => $form->id,
            'answers' => $answers ?? [
                [
                    'field_id' => $fields[Field::TYPE_TEXT]->id,
                    'value' => 'kekekeek'
                ],
                [
                    'field_id' => $fields[Field::TYPE_SELECT]->id,
                    'value' => [
                        'Значение 1', 'Значение 2'
                    ]
                ],
                [
                    'field_id' => $fields[Field::TYPE_ATTACHMENT]->id,
                    'value' => $attachment->id
                ]
            ]
        ];

        return $this->post(route('submission.store'), $data);
    }

    public function testCanStoreSubmissionAsStudent() {
        $response = $this->storeSubmission();

        $response
            ->assertSuccessful()
            ->assertJsonStructure([
            'data' => [
                'id',
                'status',
                'user_id',
                'form' => [
                    'id',
                    'title',
                    'description',
                    'fields' => [
                        [
                            'id',
                            'form_id',
                            'title'
                        ]
                    ]
                ],
                'answers' => [
                    [
                        'id',
                        'value',
                        'attachment'
                    ]
                ]
            ]
        ]);

        // Check answer foreign keys ans values

        $answers = $response->json('data.answers');

        $this->assertTrue(sizeof($answers) === 4);
    }

    public function testCanSimpleIndexAsStudent() {
         factory(Submission::class, 4)->create()->each(function(Submission $submission) {
             $submission->form->fields()->saveMany(factory(Field::class, 3)->create());
         });

        $response = $this->getJson(route('submission.index'));

        $response->assertSuccessful()
            ->assertJsonStructure([
                'page',
                'last_page',
                'page_size',
                'total_records',
                'data' => [
                    '*' => [
                        'id',
                        'form' => [
                            'id',
                            'title',
                            'description'
                        ],
                        'user_id',
                        'status',
                        'form_id'
                    ]
                ]
            ])->assertJson(['page' => 1]);
    }

    public function testCanUpdateSubmissionAsStudent() {
        $response = $this->storeSubmission();

        $fields = Field::find(array_map(function($field) {
            return $field['id'];
        }, $response->json('data.form.fields')))->keyBy('type');

        $data = [
            'answers' => [
                [
                    'field_id' => $fields[Field::TYPE_SELECT]->id,
                    'value' => ['Значение 2', 'Значение 3']
                ]
            ]
        ];

        $response = $this->patchJson(route('submission.update', [
            'submission' => $response->json('data.id')
        ]), $data);

        $response->assertSuccessful();
    }

    public function testCanRead() {
        $response = $this->storeSubmission();

        $submission = Submission::find($response->json('data.id'));

        $reply = factory(Reply::class)->create(['submission_id' => $submission->id]);

        /** @var Attachment $attachment */
        $attachment = factory(Attachment::class)->make();

        $attachment->attachable()->associate($reply);

        $attachmentResponse = $this->getJson(route('submission.show', ['submission' => $submission->id]))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'status',
                    'user_id',
                    'form' => [
                        'title',
                        'description',
                        'fields' => [
                            '*' => [
                                'title',
                                'type'
                            ]
                        ]
                    ],
                    'answers' => [
                        '*' => [
                            'field_id',
                            'value'
                        ]
                    ],
                    'replies' => [
                        '*' => [
                            'reply',
                            'attachment'
                        ]
                    ]
                ]
            ])
            ->assertJson([
                'data' => [
                    'id' => $submission->id
                ]
            ])
            ->json('data.replies.attachment');

        $this->assertEquals($attachment->id, $attachmentResponse['id']);
    }

    public function testCanDeleteSubmissionAsStudent() {
        $response = $this->storeSubmission();

        $submissionId = $response->json('data.id');

        $this->assertDatabaseHas('submissions', ['id' => $submissionId]);

        $response = $this->deleteJson(route('submission.destroy', [
            'submission' => $submissionId
        ]));

        $response->assertSuccessful();

        $this->assertDatabaseMissing('submissions', ['id' => $submissionId]);
    }
}
