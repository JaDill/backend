<?php

namespace Tests\Feature;

use App\Models\Attachment;
use App\Models\Category;
use App\Models\Course;
use App\Models\Field;
use App\Models\Form;
use App\Models\User;
use App\Models\UserSettings;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class UserTest extends TestCase
{
    private $token;

    protected function setUp()
    {
        parent::setUp();

        $this->token = auth()->login('dvgluschenko@edu.hse.ru');
    }

    public function json($method, $uri, array $data = [], array $headers = [])
    {
        $token = auth()->login('dvgluschenko@edu.hse.ru');

        $headers = array_merge(['Authorization' => $token], $headers);

        return parent::json($method, $uri, $data, $headers);
    }

    public function testCanGetUserInfo()
    {
        $this->getJson(route('user.me'))
            ->assertSuccessful()
            ->assertJson([
                'data' => [
                    'user' => [
                        'id' => 'dvgluschenko@edu.hse.ru'
                    ],
                    'settings' => [
                        'default_course' => UserSettings::find('dvgluschenko@edu.hse.ru')->default_course
                    ]
                ]
            ]);
    }

    public function testCanUpdateSettings()
    {
        $course = factory(Course::class)->create();
        $this->postJson(route('user.settings'), [
            'default_course' => $course->id
        ])
            ->assertSuccessful()
            ->assertJson([
                'data' => [
                    'default_course' => $course->id
                ]
            ]);
    }
}
