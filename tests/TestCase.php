<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\DB;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function setUp()
    {
        parent::setUp();

        $this->artisan('cache:clear');

        DB::beginTransaction();
    }

    protected function tearDown()
    {
        DB::rollBack();

        parent::tearDown();
    }
}
