<?php

namespace Tests\Unit;

use App\Extensions\Auth\RuzUserProvider;
use App\Jobs\DeleteExpiredSubmissions;
use App\Models\Attachment;
use App\Models\Field;
use App\Models\Form;
use App\Models\Submission;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class DeleteExpiredSubmissionsJobTest extends TestCase
{
    private $token;

    protected function setUp()
    {
        parent::setUp();

        $this->token = auth()->login('dvgluschenko@edu.hse.ru');
    }

    public function json($method, $uri, array $data = [], array $headers = [])
    {
        $headers = array_merge(['Authorization' => $this->token], $headers);

        return parent::json($method, $uri, $data, $headers);
    }

    private function storeSubmission(?Form $form = null, ?array $fields = null, ?array $answers = null) : TestResponse
    {
        if($form === null) {
            $form = factory(Form::class)->create();
        }

        if($fields === null) {
            $fields = [
                Field::TYPE_TEXT => factory(Field::class)->create(['form_id' => $form->id]),
                Field::TYPE_SELECT => factory(Field::class)->state('select')->create(['form_id' => $form->id]),
                Field::TYPE_ATTACHMENT => factory(Field::class)->state('attachment')->create(['form_id' => $form->id])
            ];
        } else {
            $form->fields()->saveMany($fields);
        }

        Storage::fake();

        $file = UploadedFile::fake()->image('test.png');

        $attachment = Attachment::create([
            'name' => 'image.png',
            'path' => $file->store('attachments'),
            'size' => $file->getSize(),
            'uploaded_by' => 'dvgluschenko@edu.hse.ru'
        ]);

        $data = [
            'form_id' => $form->id,
            'answers' => $answers ?? [
                    [
                        'field_id' => $fields[Field::TYPE_TEXT]->id,
                        'value' => 'kekekeek'
                    ],
                    [
                        'field_id' => $fields[Field::TYPE_SELECT]->id,
                        'value' => [
                            'Значение 1', 'Значение 2'
                        ]
                    ],
                    [
                        'field_id' => $fields[Field::TYPE_ATTACHMENT]->id,
                        'value' => $attachment->id
                    ]
                ]
        ];

        return $this->postJson(route('submission.store'), $data);
    }

    public function testCanRemoveExpiredSubmission()
    {
        $id = $this->storeSubmission()->json('data.id');

        Submission::query()->where('id', $id)->update(['delete_at' => (new Carbon)->subDays(2)]);

        $job = new DeleteExpiredSubmissions;

        $job->handle();

        $this->assertDatabaseMissing('submissions', ['id' => $id]);
    }
}
