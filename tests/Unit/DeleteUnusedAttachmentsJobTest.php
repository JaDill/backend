<?php

namespace Tests\Unit;

use App\Jobs\DeleteUnusedAttachments;
use App\Models\Attachment;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class DeleteUnusedAttachmentsJobTest extends TestCase
{
    public function testCanDeleteUnusedAttachments()
    {
        Storage::fake();

        /** @var Collection $attachments */
        $attachments = factory(Attachment::class, 10)->create();

        $shouldBeRemoved = $attachments->random(3)->map(function(Attachment $attachment) {
            $attachment->created_at = (new Carbon)->subHour(2);

            $attachment->save();

            return $attachment->id;
        })->toArray();

        $rest = array_diff($attachments->pluck('id')->toArray(), $shouldBeRemoved);

        $job = new DeleteUnusedAttachments;

        $job->handle();

        $this->assertTrue(Attachment::query()->whereIn('id', $shouldBeRemoved)->doesntExist());
        $this->assertTrue(Attachment::query()->whereIn('id', $rest)->count() === 7);
    }
}
