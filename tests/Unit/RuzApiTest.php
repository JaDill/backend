<?php

namespace Tests\Unit;

use App\Services\RuzApiService;
use Tests\TestCase;

class RuzApiTest extends TestCase
{
    /** @var RuzApiService $apiService */
    private $apiService;

    protected function setUp()
    {
        parent::setUp();

        $this->apiService = new RuzApiService();
    }

    public function testCanGetStudentInfoByEmail() {
        $email = 'dvgluschenko@edu.hse.ru';
        $data = $this->apiService->getStudentInfo($email);

        $this->assertEquals($data['email'], $email);
        $this->assertEquals($data['type'], 'student');
    }

    public function testCanGetStudentInfoById() {
        $id = "131517";

        $data = $this->apiService->getStudentInfo($id);

        $this->assertEquals($data['id'], $id);
        $this->assertEquals($data['email'], 'dvgluschenko@edu.hse.ru');
        $this->assertEquals($data['type'], 'student');
    }
}
