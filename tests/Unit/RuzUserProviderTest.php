<?php

namespace Tests\Unit;

use App\Extensions\Auth\RuzUserProvider;
use Tests\TestCase;

class RuzUserProviderTest extends TestCase
{
    const EMAIL = 'dvgluschenko@edu.hse.ru';

    /** @var RuzUserProvider $provider */
    private $provider;

    protected function setUp()
    {
        parent::setUp();

        $this->provider = $this->app->make(RuzUserProvider::class);
    }

    public function testCanGetStudentWithCredentials() {
        $credentials = [
            'id' => self::EMAIL
        ];

        $user = $this->provider->retrieveByCredentials($credentials);

        $this->assertNotNull($user);
        $this->assertEquals($user->getAuthIdentifier(), self::EMAIL);
    }

    public function testCanGetStudentById() {
        $user = $this->provider->retrieveById(self::EMAIL);

        $this->assertNotNull($user);
        $this->assertEquals($user->getAuthIdentifier(), self::EMAIL);
    }
}
